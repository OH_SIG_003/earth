void foo (Blat *bar) {
    char *blat;
    int *ding = nullptr;
    blat = f(x);
    if (bar != nullptr) {
        *bar = blat;
        printf (*blat);
    }
    bar -> x = 99; 
    if (*ding == 0) {
        bar -> x = 42;
    }
    f (bah[x].z -> y);
}

void foo2 (Blat *bar) {
    char *blat;
    int *ding = nullptr;
    blat = f(x);

    if (bar != nullptr) {
        *bar = blat;
        printf (*blat);
    }

    bar -> x = 99; 
    if (*ding == 0) {
        bar -> x = 42;
    }

    CHECK_NULL_VOID (bah[x].z);
    f (bah[x].z -> y);

    if (blat != nullptr) {
        *bar = blat;
        printf (*blat);
    }

    if ((bar != nullptr) && (blat != nullptr)) {
        *bar = blat;
        printf (*blat);
    }

    if (bar == nullptr) {
        flagerror ();
    } else {
        noerror (*bar);
        noerror (bar -> x);
    }

    if (blat == nullptr) {
        flagerror ();
    }
    noerror (blat -> z);
}
