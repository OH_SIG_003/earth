#!/bin/sh
ulimit -s hard

# Check argument
if [ "$1" = "" ]; then 
    echo "Usage:  testall DIR"
    echo "  (where DIR is a directory tree containing TS files named using .cpp)"
    exit 99
fi

# Clean up previous results
./cleanall.sh "$1"

# Find C++ files (only those named *.cpp)
for i in `find ${1} -type f | egrep -e '\.cpp$' | egrep -v '\.audit\.cpp$'` ; do
    echo "=== $i ===" 
    (txl -q $i -o $i.audit.cpp cppaudit.txl $2 $3 $4 $5)
    cat $i.audit.cpp
done

