## HPAuditor: 
Analyzes TypeScript files for potential coding issues using the tsaudit tool, displays the origin file and the new file in a File Comparison, and presents the findings in a tool window.

### Main Steps:

* Analysis Trigger: User runs the plugin on a .ts file by a keyboard shortcut (Ctrl+Alt+A) or menu option to initiate the analysis.
* Validation & Execution: Tool validates file type and runs tsaudit.exe.
* Parsing & Report Generation: Analyzes output, extracts issue information, and generates a .ts.audit.ts report file.
* Presentation & Navigation: Opens the report alongside the original file and offers navigation to specific issues.
  
### Additional Features:

Tool Window: Provides a dedicated UI with:
* Statistics Panel: Displays totals for defects, errors, warnings, and infos.
* Filterable List: Shows issue descriptions with rule IDs (clickable for line jump).
* Reference Panel: Place for the reference document (implement later).

### Plugin Configuration File:

The provided configuration file defines key characteristics of the plugin for JetBrains IDE integration:

* ID: com.example.HPAuditor - identifies the plugin uniquely and cannot be changed across versions.
* Name: HPAuditor - displayed name for users.
* Vendor: identifies the plugin author.
* Description: Explains the plugin's purpose with basic HTML formatting.
* Depends: Requires the com.intellij.modules.platform dependency.
* Extension Points:
  * Registers a tool window HPAuditor with:
    * Icon: /icons/Performance.svg.
    * Position: Bottom of the IDE window.
    * Factory class: com.example.devecohp.MyToolWindowFactory.
    * Actions: Defines an action auditor triggered by:
      * Keyboard shortcut: Ctrl+Alt+A.
      * Menu option: "HPAuditor".
      * Class: com.example.devecohp.auditor.

### Class and Function Descriptions:

**auditor** : Main class handling analysis execution, report generation, and navigation.
* getAuditList(): Returns list of all identified issues with details.
* getDefects(): Returns total number of defects (errors + warnings + infos).
* getWarnings(): Returns number of identified warnings.
* getErrors(): Returns number of identified errors.
* getInfos(): Returns number of informational messages.
* getName(): Returns file name from where analysis was triggered.
* setName(String f): Sets the file name for the generated audit report.
* getFileName(): Returns the name of the generated audit report file with the full path.
* setFileName(String f): Sets the name of the generated audit report file with the full path.
* actionPerformed(AnActionEvent e): Main entry point for triggering analysis, processing results, and opening the report.
* jump(int lineNum): Navigates to the specified line number in the report file.
* openFileInSameWindow(VirtualFile newFile, Project project, PsiFile oldFile): Opens the report file alongside the original file and highlights differences.

**MyToolWindowFactory**:
* createToolWindowContent(@NotNull Project project, @NotNull ToolWindow toolWindow): Builds the UI for the tool window, including:
  * Statistics Panel: Shows current defect, error, warning, and info counts.
  * Filterable List: Displays and filters audit entries (clickable for line jump).
  * Reference Panel: Placeholder for future use.
* filterUpper(): Applies filter based on user input in the list search field.
* updateUpperPanel(): Updates UI elements with new analysis results after running tsaudit.exe.
