## TODO List for plugin
1. Improve UI
2. "Ignore whitespaces and empty lines" has bug in deveco but works in intellij (disappeared)
3. Complete all rules documents and put them into docs folder in resources 
4. Everytime click the content to jump to certain line number, will create a new File Comparsion 
5. Documents show in the tool window will go to the bottom of the file. 
6. Update and finish the id, name, vendor, description in the plugin.xml file
7. Show a successful window in the right corner.
8. Make the tool window is updated in between projects 
9. Add the icon on menu
10. change hpauditor.exe file to something runnable in mac
11. add support for c/cpp
12. add language option
13. defult ignore white space and lines
14. allow user run hpaudit on all ts & ets file in a directory
15. change the letter on language option
16. allow the user to click on lightbub on the left of the file to show suggestion
17. add authors