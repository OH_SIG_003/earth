package com.example.devecohp;

import com.intellij.codeInsight.daemon.LineMarkerInfo;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.*;

public class AuditorTool {
    public void auditFile(PsiFile file,String auditorFolderPath){
        Project project = file.getProject();
        Auditor.runAndCreatePageInfo(project,file,auditorFolderPath);
        PageManager.getInstance(project).setModeReplace();
    }
    public void auditFile(String fullPath,Project project,String auditorFolderPath){
        auditFile(getPsiFileFromFullPath(fullPath,project),auditorFolderPath);
    }
    public Map<Integer,List<String>> getLinesForRules(PsiFile psiElement){
        return AuditorLineMarker.processFile(psiElement);
    }
    public void auditFiles(List<PsiFile> allFiles, Project project,String auditorFolderPath){

        AuditAllFiles.auditAllFiles(allFiles,project,auditorFolderPath);
    }
    public void auditFiles(List<PsiFile> allFiles,String auditorFolderPath){
        Map<Project,List<PsiFile>> map = new HashMap<>();
        for(PsiFile psiFile: allFiles){
            if(!map.containsKey(psiFile.getProject())){
                map.put(psiFile.getProject(),new ArrayList<>());
            }
            map.get(psiFile.getProject()).add(psiFile);
        }
        for(Project project: map.keySet()){
            AuditAllFiles.auditAllFiles(map.get(project),project,auditorFolderPath);
        }
    }
    public void auditFiles(String[] fullPaths,Project project,String auditorFolderPath){
        List<PsiFile> allFiles = new ArrayList<>();
        for(String path :fullPaths){
            allFiles.add(getPsiFileFromFullPath(path,project));
        }
        AuditAllFiles.auditAllFiles(allFiles,project,auditorFolderPath);
    }
    public void auditDictionary(PsiDirectory directory,String auditorFolderPath){
        Project project = directory.getProject();
        List<PsiFile> allFiles = AuditAllFiles.getAllFiles(directory);
        auditFiles(allFiles,project,auditorFolderPath);
    }
    public void auditDictionary(String fullPath,Project project,String auditorFolderPath){
        VirtualFile virtualFile = LocalFileSystem.getInstance().findFileByIoFile(new File(fullPath));
        List<PsiFile> allFiles = AuditAllFiles.getAllFiles(PsiManager.getInstance(project).findDirectory(virtualFile));
        auditFiles(allFiles,project,auditorFolderPath);
    }
    public PsiFile getPsiFileFromFullPath(String fullPath,Project project){
       return PsiManager.getInstance(project).findFile(LocalFileSystem.getInstance().findFileByIoFile(new File(fullPath)));
    }
}
