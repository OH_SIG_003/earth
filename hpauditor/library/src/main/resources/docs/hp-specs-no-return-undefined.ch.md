## 不要返回未定义


不像静态类型语言强制要求函数返回一个指定类型的值，JavaScript允许在一个函数中不同的代码路径返回不同类型的值。

而JavaScript在以下情况下函数会返回undefined：

1. 在退出之前没有执行return语句
1. 执行return语句，但没有显式地指定一个值
1. 执行return undefined
1. 执行return void，其后跟着一个表达式 (例如，一个函数调用)
1. 执行return，其后跟着其它等于undefined的表达式

在一个函数中，如果任何代码路径显式的返回一个值，但一些代码路径不显式返回一个值，那么这种情况可能是个书写错误，尤其是在一个较大的函数里。因此，函数内，应使用一致的return语句。

**【反例】**

```javascript
function doSomething(condition) {
  if (condition) {
    ...
    return true;
  } else {
    ...
    return;
  }
}
function doSomething(condition) {
  if (condition) {
    ...
    return true;
  }
}
```

**【正例】**

```javascript
// 保证所有路径都以相同的方式返回值
function doSomething(condition) {
  if (condition) {
    ...
    return true;
  } else {
    ...
    return false;
  }
}

function doSomething(condition) {
  if (condition) {
    ...
    return true;
  }
  ...
  return false;
}
```