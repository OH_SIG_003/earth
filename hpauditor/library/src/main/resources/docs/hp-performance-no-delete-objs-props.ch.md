## 不要删除对象的可计算属性


delete会改变对象的布局，而delete对象的可计算属性会非常危险，而且会大幅约束语言运行时的优化从而影响执行性能。

注意：建议不删除对象的任何属性，如果有需要，建议用map和set。

**【反例】**

```javascript
// Can be replaced with the constant equivalents, such as container.aaa
delete container['aaa'];

// Dynamic, difficult-to-reason-about lookups
const name = 'name';
delete container[name];
delete container[name.toUpperCase()];
```

**【正例】**

```javascript
// 一定程度也会影响优化性能，但比删除可计算属性好一些。
delete container.aaa;

delete container[7];
```