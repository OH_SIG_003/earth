## 声明变量时要求使用const或let而不是var


在ECMAScript 6允许开发者使用let和const关键字在块级作用域而非函数作用域下声明变量。块级作用域在很多其他编程语言中很普遍，能帮助开发者避免错误。只读变量用const定义，其它变量用let定义。

**【反例】**

```javascript
var number = 1;
var count = 1;
if (isOK) {
  count += 1;
}
```

**【正例】**

```javascript
const number = 1;
let count = 1;
if (isOK) {
  count += 1;
}
```