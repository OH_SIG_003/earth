## 声明参数要和实际的参数一致

声明的参数要和实际的传入参数个数及类型一致，如果不传入参数，则会作为undefined处理，可能造成与实际入参类型不匹配的情况，从而导致运行时走入慢速路径，影响性能。

【反例】

``` TypeScript
function add(a: number, b: number) {
  return a + b;
}
// 参数个数是2，不能给3个
add(1, 2, 3);
// 参数个数是2，不能给1个
add(1);
// 参数类型是number，不能给string
add("hello", "world");
```

【正例】

``` TypeScript
function add(a: number, b: number) {
  return a + b;
}
// 按照函数参数个数及类型要求传入参数
add(1, 2);