## 避免使用type类型标注

如果传入的参数类型是type类型，实际入参可能是一个object literal，也可能是一个class，编译器及虚拟机因为类型不固定，无法做编译期假设进而进行相应的优化。

【反例】

``` TypeScript
// type类型无法在编译期确认, 可能是一个object literal，也可能是另一个class Person
type Person = {
  name: string;
  age: number;
};
 
function greet(person: Person) {
  return "Hello " + person.name;
}

// type方式是不推荐的，因为其有如下两种使用方式，type类型无法在编译期确认
// 调用方式一
class O1 {
  name: string = "";
  age: number = 0;
}
let objectliteral: O1 = {name : "zhangsan", age: 20 };
greet(objectliteral);

// 调用方式二
class Person {
  name: string = "zhangsan";
  age: number = 20;
}
let person = new Person();
greet(person);
```

【正例】

``` TypeScript
interface Person {
  name: string ;
  age: number;
}
 
function greet(person: Person) {
  return "Hello " + person.name;
}

class Person {
  name: string = "zhangsan";
  age: number = 20;
}

let person = new Person();
greet(person);
```