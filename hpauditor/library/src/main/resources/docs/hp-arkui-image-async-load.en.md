# Reduce layout time

Reducing layout time can be achieved by asynchronous loading and reducing the view nesting level.

## Asynchronous loading

Due to the synchronous loading operation, the image creation task needs to be completed on the main thread, and the page layout Layout needs to wait for the execution of the image creation makePixelMap task, resulting in extended layout time. On the contrary, the asynchronous loading operation is completed in other threads and starts at the same time as the page layout, and does not hinder the page layout, so the page layout is faster and the performance is better. However, not all loading must use asynchronous loading. It is recommended to set syncLoad to true when loading local images with smaller sizes. Because it takes less time, it can be executed on the main thread.

Case: Use the Image component to synchronously load high-resolution images, blocking the UI thread and increasing the total page layout time.

```
@Entry
@Component
struct SyncLoadImage {
  @State arr: String[] = Array.from(Array(100), (val,i) =>i.toString());
  build() {
    Column() {
      Row() {
        List() {
          ForEach(this.arr, (item: string) => {
            ListItem() {
              Image($r('app.media.4k'))
                .border({ width: 1 })
                .borderStyle(BorderStyle.Dashed)
                .height(100)
                .width(100)
                .syncLoad(true)
            }
          }, (item: string) => item.toString())
        }
      }
    }
  }
}
```

Optimization: Use the default asynchronous loading method of the Image component to load images, without blocking the UI thread and reducing page layout time.

```
@Entry
@Component
struct AsyncLoadImage {
  @State arr: String[] = Array.from(Array(100), (val,i) =>i.toString());
    build() {
      Column() {
        Row() {
          List() {
            ForEach(this.arr, (item: string) => {
              ListItem() {
                Image($r('app.media.4k'))
                  .border({ width: 1 })
                  .borderStyle(BorderStyle.Dashed)
                  .height(100)
                  .width(100)
              }
            }, (item: string) => item.toString())
          }
        }
      }
  }
}
```

