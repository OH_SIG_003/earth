## 条件渲染和容器限制

针对反复切换条件渲染的控制分支，但切换项仅涉及页面中少部分组件的场景，下面示例通过Column父组件下1000个Text组件，与1个受条件渲染控制的Text组件的组合来说明该场景，并对1个受条件渲染控制的Text组件的外面是否加上容器组件做包裹，做两种情况的正反例性能数据的对比。

**反例**

没有使用容器限制条件渲染组件的刷新范围，导致条件变化会触发创建和销毁该组件，影响该容器内所有组件都会刷新。

```ts
@Entry
@Component
struct RenderControlWithoutStack {
  @State isVisible: boolean = true;
  private data: number[] = [];

  aboutToAppear() {
    for (let i: number = 0; i < 1000; i++) {
      this.data.push(i);
    }
  }

  build() {
    Column() {
      Stack() {
        Scroll() {
          Column() { // 刷新范围会扩展到这一层
            if (this.isVisible) { // 条件变化会触发创建和销毁该组件，影响到容器的布局，该容器内所有组件都会刷新
              Text('New item').fontSize(20)
            }
            ForEach(this.data, (item: number) => {
              Text(`Item value: ${item}`).fontSize(20).width('100%').textAlign(TextAlign.Center)
            }, (item: number) => item.toString())
          }
        }
      }.height('90%')

      Button('Switch Hidden and Show').onClick(() => {
        this.isVisible = !(this.isVisible);
      })
    }
  }
}
```

**正例**

使用容器限制条件渲染组件的刷新范围。

```ts
@Entry
@Component
struct RenderControlWithStack {
  @State isVisible: boolean = true;
  private data: number[] = [];

  aboutToAppear() {
    for (let i: number = 0; i < 1000; i++) {
      this.data.push(i);
    }
  }

  build() {
    Column() {
      Stack() {
        Scroll() {
          Column() {
            Stack() { // 在条件渲染外套一层容器，限制刷新范围
              if (this.isVisible) {
                Text('New item').fontSize(20)
              }
            }

            ForEach(this.data, (item: number) => {
              Text(`Item value: ${item}`).fontSize(20).width('100%').textAlign(TextAlign.Center)
            }, (item: number) => item.toString())
          }
        }
      }.height('90%')

      Button('Switch Hidden and Show').onClick(() => {
        this.isVisible = !(this.isVisible);
      })
    }
  }
}
```
**效果对比**

正反例相同的操作步骤：通过点击按钮，将初始状态为显示的Text组件切换为隐藏状态，再次点击按钮，将隐藏状态切换为显示状态。两次切换间的时间间隔长度，需保证页面渲染完成。

容器内有Text组件被if条件包含，if条件结果变更会触发创建和销毁该组件，此时影响到父组件Column容器的布局，该容器内所有组件都会刷新，包括模块ForEach，因此导致主线程UI刷新耗时过长。