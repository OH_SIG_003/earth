## 避免动态添加属性

对象在创建的时候，如果开发者明确后续还需要添加属性，可以提前置为undefined。动态添加属性会导致对象布局变化，影响编译器和运行时优化效果。

【反例】

``` TypeScript
// 后续obj需要再添加z属性
class O1 {
  x: string = "";
  y: string = "";
}
let obj: O1 = {"x": xxx, "y": "yyy"};
...
// 这种动态添加方式是不推荐的
obj.z = "zzz";
```

【正例】

``` TypeScript
class O1 {
  x: string = "";
  y: string = "";
  z: string = "";
}
let obj: O1 = {"x": "xxx", "y": "yyy", "z": ""};
...
obj.z = "zzz";
```