% TXL String Utilities
% J.R. Cordy, July 2022

#ifndef STRINGUTILS

#define STRINGUTILS

% Utility string function - s/Pattern/Replacement/
function subst Pattern [stringlit] Replacement [stringlit]
    replace [stringlit]
        S [stringlit]
    construct PatternIndex [number]
        _ [index S Pattern]
    deconstruct not PatternIndex
        0
    construct PatternIndexMinus1 [number]
        PatternIndex [- 1]
    construct PrePattern [stringlit]
        S [: 1 PatternIndexMinus1]
    construct PatternIndexPlusLength [number]
        _ [# Pattern] [+ PatternIndex]
    construct PostPattern [stringlit]
        S [: PatternIndexPlusLength 99999]
    by
        PrePattern [+ Replacement] [+ PostPattern]
end function

% Utility string function - s/Pattern/Replacement/g
function substglobal Pattern [stringlit] Replacement [stringlit]
    replace [stringlit]
        S [stringlit]
    construct PatternIndex [number]
        _ [index S Pattern]
    deconstruct not PatternIndex
        0
    construct PatternIndexMinus1 [number]
        PatternIndex [- 1]
    construct PrePattern [stringlit]
        S [: 1 PatternIndexMinus1]
    construct PatternIndexPlusLength [number]
        _ [# Pattern] [+ PatternIndex]
    construct PostPattern [stringlit]
        S [: PatternIndexPlusLength 99999]
          [substglobal Pattern Replacement]
    by
        PrePattern [+ Replacement] [+ PostPattern]
end function

% Utility string function - s/^Pattern/Replacement/
function substleft Pattern [stringlit] Replacement [stringlit]
    replace [stringlit]
        S [stringlit]
    construct PatternIndex [number]
        _ [index S Pattern]
    deconstruct PatternIndex
        1
    construct PatternLengthPlusOne [number]
        _ [# Pattern] [+ 1]
    construct PostPattern [stringlit]
        S [: PatternLengthPlusOne 99999]
    by
        Replacement [+ PostPattern]
end function

% Utility string function - s/Pattern$/Replacement/
function substright Pattern [stringlit] Replacement [stringlit]
    replace [stringlit]
        S [stringlit]
    construct SLength [number]
        _ [# S]
    construct PatternLength [number]
        _ [# Pattern]
    construct PatternIndex [number]
        SLength [- PatternLength] [+ 1]
    construct STail [stringlit]
        S [: PatternIndex 99999]
    deconstruct STail
        Pattern
    construct PatternIndexMinus1 [number]
        PatternIndex [- 1]
    construct PrePattern [stringlit]
        S [: 1 PatternIndexMinus1]
    by
        PrePattern [+ Replacement]
end function

% Utility string function - how many instances of Pattern in S?
function count Pattern [stringlit] S [stringlit]
    construct PatternIndex [number]
        _ [index S Pattern]
    deconstruct not PatternIndex
        0
    construct PatternIndexPlusOne [number]
        PatternIndex [+ 1]
    construct RestS [stringlit]
        S [: PatternIndexPlusOne 99999]
    replace [number]
        N [number]
    by
        N [+ 1]
          [count Pattern RestS]
end function

% Utility string function - [grep] with ^ and $ detection
function egrep Pattern [stringlit]
    match [stringlit]
        String [stringlit]
    construct EString [stringlit]
        _ [+ "^"] [+ String] [+ "$"]
    where
        EString [grep Pattern]
end function

#endif STRINGUTILS
