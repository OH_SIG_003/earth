## 禁止使用with() {}


使用with让代码在语义上变得不清晰，因为with的对象，可能会与局部变量产生冲突，从而改变程序原本的用义。

**【反例】**

```javascript
const foo = { x: 5 };
with (foo) {
  let x = 3;
  console.log(x);  // x = 3
}
console.log(foo.x);  // x = 3
```