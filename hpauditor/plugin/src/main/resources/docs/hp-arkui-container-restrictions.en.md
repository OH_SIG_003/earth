# Applicable scene

Through conditional rendering or visibility control, you can switch between showing and hiding components. The applicable scenarios for both are as follows:

## Applicable scenarios for conditional rendering:

- During the application cold start phase, when the application loads and draws the homepage, if the component does not need to be displayed initially, it is recommended to use conditional rendering instead of display and hide control to reduce rendering time and speed up startup.
- If the component does not switch between display and hide frequently, or does not need to be displayed most of the time, it is recommended to use conditional rendering instead of display and hide control to reduce interface complexity, reduce nesting levels, and improve performance.
- If the controlled component occupies a large amount of memory and developers prioritize memory, it is recommended to use conditional rendering instead of display and hide control to instantly destroy components that do not need to be displayed and save memory.
- If the component subtree structure is complex and the control branch of conditional rendering is repeatedly switched, it is recommended to use conditional rendering in conjunction with the component reuse mechanism to improve application performance.
- If the switching item only involves some components, and the control branch of conditional rendering is repeatedly switched, it is recommended to use conditional rendering in conjunction with container restrictions to accurately control the scope of component updates and improve application performance.

## Applicable scenarios for explicit and implicit control:

- If a component frequently switches between showing and hiding, it is recommended to use visibility control instead of conditional rendering to avoid frequent creation and destruction of components and improve performance.
- If the component needs to maintain its placeholder position in the page layout after it is hidden, it is recommended to apply visibility control.

## Conditional rendering and container restrictions

For the scenario where the control branch of conditional rendering is repeatedly switched, but the switching item only involves a small number of components on the page, the following example illustrates this scenario by combining 1000 Text components under the Column parent component and 1 Text component controlled by conditional rendering. , and compare whether a Text component controlled by conditional rendering is wrapped with a container component, and compare the positive and negative performance data of the two situations.

Counterexample

Failure to use container constraints to render the refresh scope of the component causes condition changes to trigger the creation and destruction of the component, affecting all components in the container to be refreshed.

```
@Entry
@Component
struct RenderControlWithoutStack {
  @State isVisible: boolean = true;
  private data: number[] = [];

  aboutToAppear() {
    for (let i: number = 0; i < 1000; i++) {
      this.data.push(i);
    }
  }

  build() {
    Column() {
      Stack() {
        Scroll() {
          Column() { // 刷新范围会扩展到这一层
            if (this.isVisible) { // 条件变化会触发创建和销毁该组件，影响到容器的布局，该容器内所有组件都会刷新
              Text('New item').fontSize(20)
            }
            ForEach(this.data, (item: number) => {
              Text(`Item value: ${item}`).fontSize(20).width('100%').textAlign(TextAlign.Center)
            }, (item: number) => item.toString())
          }
        }
      }.height('90%')

      Button('Switch Hidden and Show').onClick(() => {
        this.isVisible = !(this.isVisible);
      })
    }
  }
}
```

Positive example

Render the component's refresh scope using container constraints.

```
@Entry
@Component
struct RenderControlWithStack {
  @State isVisible: boolean = true;
  private data: number[] = [];

  aboutToAppear() {
    for (let i: number = 0; i < 1000; i++) {
      this.data.push(i);
    }
  }

  build() {
    Column() {
      Stack() {
        Scroll() {
          Column() {
            Stack() { // 在条件渲染外套一层容器，限制刷新范围
              if (this.isVisible) {
                Text('New item').fontSize(20)
              }
            }

            ForEach(this.data, (item: number) => {
              Text(`Item value: ${item}`).fontSize(20).width('100%').textAlign(TextAlign.Center)
            }, (item: number) => item.toString())
          }
        }
      }.height('90%')

      Button('Switch Hidden and Show').onClick(() => {
        this.isVisible = !(this.isVisible);
      })
    }
  }
}
```

