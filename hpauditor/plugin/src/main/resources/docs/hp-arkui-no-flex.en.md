If developers use low-performance code to implement functional scenarios, it may not affect the normal operation of the application, but it will have a negative impact on the performance of the application. This chapter lists some scenarios that can improve performance for developers' reference to avoid performance degradation caused by application implementation.

# Use Column/Row instead of Flex

Since Flex container components have shrink by default, which leads to secondary layout, this will cause performance degradation in page rendering to a certain extent.

```
@Entry
@Component
struct MyComponent {
  build() {
    Flex({ direction: FlexDirection.Column }) {
      Flex().width(300).height(200).backgroundColor(Color.Pink)
      Flex().width(300).height(200).backgroundColor(Color.Yellow)
      Flex().width(300).height(200).backgroundColor(Color.Grey)
    }
  }
}
```

The above code can replace Flex with Column and Row, and avoid the negative impact of Flex's secondary layout while ensuring the same page layout effect.

```
@Entry
@Component
struct MyComponent {
  build() {
    Column() {
      Row().width(300).height(200).backgroundColor(Color.Pink)
      Row().width(300).height(200).backgroundColor(Color.Yellow)
      Row().width(300).height(200).backgroundColor(Color.Grey)
    }
  }
}
```
