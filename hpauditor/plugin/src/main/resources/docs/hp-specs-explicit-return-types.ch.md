## 必须显式声明函数及类方法的返回值类型


显式声明返回类型，这可确保返回值被分配给正确类型的变量；或者在没有返回值的情况下，调用代码不会尝试把undefined分配给变量。

**【反例】**

```javascript
// 没有返回值时，没有声明返回值类型为void
function test() {
  return;
}
// 没有声明返回值类型为number
function fn() {
  return 1;
};
// 没有声明返回值类型为string
let arrowFn = () => 'test';
class Test {
  // 没有返回值时，没有声明返回值类型为void
  method() {
    return;
  }
}
```
**【正例】**

```javascript
// 函数没有返回值时，显式声明返回值类型为void
function test(): void {
  return;
}
// 显式声明返回值类型为number
function fn(): number {
  return 1;
};
// 显式声明返回值类型为 string
let arrowFn = (): string => 'test';
class Test {
  // 没有返回值时，显式声明返回值类型为void
  method(): void {
    return;
  }
}
```