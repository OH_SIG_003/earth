Bad ts code
```
class A {
  private a: number | string;
  private b: number | string;
  private c: number | string;
  
  constructor(a: number, b: number, c: number) {
    this.a = a;
    this.b = b;
    this.c = c;
  }
}

// a, b, c are initialized as strings, does not match type annotation.
let a = new A('a', 'b', 'c');
console.log(a);
```
Suggested example
```
class A {
  private a: number | string;
  private b: number | string;
  private c: number | string;
  
  constructor(a: number, b: number, c: number) {
    this.a = a;
    this.b = b;
    this.c = c;
  }
}

let a = new A(0, 0, 0);
console.log(a);
```