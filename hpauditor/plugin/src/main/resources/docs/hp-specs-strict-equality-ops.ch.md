## 判断相等时应使用===和!== ，而不是==和!=


JavaScript中使用双等==做相等判断时会自动做类型转换，如：[] == false、[] == ![]、3 == '03'都是true，当类型确定时使用全等===做比较可以提高效率。

**【反例】**

```javascript
age == bee
foo == true
bananas != 1
value == undefined
typeof foo == 'undefined'
'hello' != 'world'
0 == 0
true == true
```

**【正例】**

```javascript
age === bee
foo === true
bananas !== 1
value === undefined
typeof foo === 'undefined'
'hello' !== 'world'
0 === 0
true === true
```

**【例外】**

```javascript
//当判断对象是否是null的时候，可直接使用如下形式：
obj == null
obj != null
```