/* HPAudit: Always use strict mode (fixed) : hp-specs-use-strict-mode : 1 : 0 : samples/Native/entry/src/main/cpp/types/libentry/index.d.ts */
"use strict";
export const add: (a: number, b: number) => number;
export const nativeCallArkTS: (a: Function) => string;
