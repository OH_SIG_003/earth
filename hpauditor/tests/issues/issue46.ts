// If any code paths explicitly return a value, each code paths should also return a value.

// The following are examples of functions possibly returning undefined.
// 1. Functions returning a boolean
function largerThan(a: number, b: number): boolean | undefined {
  if (a > b) {
    return true;
  } else if (a === b) {
    return; // value not specified for return, returns undefined
  } else {
    return undefined; // returns undefined
  }
}

function isHappy(happy: boolean): boolean | undefined {
  if (happy) {
    return true;
  }
  return void 0; // returns undefined
}

// 2. Functions returning multiple types
function getRandom(num: number): number | string | undefined {
  if (num > 100) {
    return 50;
  } else if (num > 50) {
    return 'Hi!';
  }
  // no return statement, returns undefined
} 

console.log(largerThan(2, 1));
console.log(largerThan(1, 2));
console.log(largerThan(2, 2));
console.log(isHappy(false));
console.log(getRandom(101));
console.log(getRandom(51));
console.log(getRandom(1));
