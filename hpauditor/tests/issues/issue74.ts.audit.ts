/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue74.ts */
"use strict";
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 1 : issue74.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 1 : issue74.ts */
const USERNAME: string = 'Jack'
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 2 : issue74.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 2 : issue74.ts */
const BIRTHDAY: string = '1997-09-01'
console.log(`${username}'s birthday is ${birthday}.`)
