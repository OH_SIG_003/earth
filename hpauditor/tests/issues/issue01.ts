// Do not omit 0s before and after the decimal point of a floating point number
let n1 = .1;
let n2 = 1.;
let n3 = .1e1;
let n4 = 1.e1;
