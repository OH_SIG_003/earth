/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue310.ts */
"use strict";
//310.ts
enum TransitionEffect { ONE, TWO }
function getIconFallTransitionEffect(): TransitionEffect {
  return TransitionEffect.ONE;
}
function getTransitionEffect(para: string): TransitionEffect {
  switch (para) {case 'fall':
      return getIconFallTransitionEffect();
    default:
      break;
  }
  return TransitionEffect.TWO;
}
console.log(getTransitionEffect('fall'))
console.log(getTransitionEffect('spring'))
