/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue40.ts */
"use strict";
// An array should only be used store an ordered (indexed) set of data.
// Initializing an array
const myHash: string[] = [];

// Adding key-value pairs to array
/* HPAudit: Do not use non-numeric properties on arrays : hp-specs-no-non-numeric-props-arrays : 1 : 6 : issue40.ts */
myHash['key1'] = 'val1'; // flag this!
/* HPAudit: Do not use non-numeric properties on arrays : hp-specs-no-non-numeric-props-arrays : 1 : 7 : issue40.ts */
myHash['key2'] = 'val2'; // flag this!

// Adding a string as first element of array
myHash[0] = '222';
for (const key in myHash) {
  
  // The value of key is 0 key1 key2
  console.log(key);
}
console.log(myHash);
console.log('Length of array: ', myHash.length); //The array length is 1.


// Make sure that we don't flag objects as arrays
class HashClass {
  value: number = 0;
}
const yourHash: HashClass = { 
    value: 42
  };
/* HPAudit: Use dot notation to access object properties : hp-specs-dot-notation-for-obj-props : 1 : 25 : issue40.ts */
yourHash['value'] = 333; // do not flag this!
console.log(yourHash);
