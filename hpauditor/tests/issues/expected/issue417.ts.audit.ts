/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue417.ts */
"use strict";
export class ComponentMethodManager<T extends object> {
  /* HPAudit: Do not use indexed containers as maps : hp-performance-proper-data-structures : 1 : 2 : issue417.ts */
  private methodMap: { 
      [key: string]: Function | undefined
    } = {};

  // @ts-expect-error Ignore type compile error, functions is ok
  call<MethodT extends keyof T>(method: MethodT, ... args: Parameters<T[MethodT]>): ReturnType<T[MethodT]> {
    return this.methodMap[method as unknown as string]!( ... args);
  }
}
