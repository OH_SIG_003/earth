/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue479.ts */
"use strict";
function foo(): Boolean {
  let isNoError = 0
  /* HPAudit: Use strict equality operators : hp-specs-strict-equality-ops : 1 : 3 : issue479.ts */
  if (isNoError === 11) {
    return true
  } else {
    isNoError = 10
  }
  /* HPAudit: Do not return undefined : hp-specs-no-return-undefined : 1 : 3 : issue479.ts */
}
function foo(): Boolean {
  let isNoError = 10
  /* HPAudit: Use strict equality operators : hp-specs-strict-equality-ops : 1 : 12 : issue479.ts */
  if (isNoError === 11) {
    return true
  } else {
    isNoError = 12
    /* HPAudit: Do not return undefined : hp-specs-no-return-undefined : 1 : 16 : issue479.ts */
    return void foo()
  }
}
