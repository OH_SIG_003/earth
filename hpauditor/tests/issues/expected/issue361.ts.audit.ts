/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue361.ts */
"use strict";
/* HPAudit: Do not use eval() : hp-specs-no-eval : 1 : 1 : issue361.ts */
console.log(eval('{ a: 2 }')); // outputs 2
/* HPAudit: Do not use eval() : hp-specs-no-eval : 1 : 2 : issue361.ts */
console.log(eval('let value = 1 + 1;')); // outputs undefined
async function foo(): Promise<number> {
  /* HPAudit: Do not use return await outside try catch : hp-specs-no-return-await : 1 : 5 : issue361.ts */
  /* HPAudit: Do not use eval() : hp-specs-no-eval : 1 : 5 : issue361.ts */
  /* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 5 : issue361.ts */
  const baz: any = eval('let a=88; console.log(a*a);');
  return baz;
}
foo()
