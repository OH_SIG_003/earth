/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue478.ts */
"use strict";
class Test {
  public async access(uri: string): Promise<boolean> {
    /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 3 : issue478.ts */
    const existJudgment = await FileAccessHelper.clientManager.access(uri);
    return Promise.resolve(existJudgment);
  }
}
