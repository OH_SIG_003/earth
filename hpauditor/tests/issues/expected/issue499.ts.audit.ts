/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue499.ts */
"use strict";
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 1 : issue499.ts */
function foo(f: boolean) {
  if (f) {
    /* HPAudit: Do not dynamically declare functions and classes : hp-performance-no-dynamic-cls-func : 1 : 3 : issue499.ts */
    return class Add {};
  } else {
    /* HPAudit: Do not dynamically declare functions and classes : hp-performance-no-dynamic-cls-func : 1 : 5 : issue499.ts */
    return class Sub {};
  }
}
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 9 : issue499.ts */
function bar(b: boolean) {
  if (b) {
    /* HPAudit: Do not dynamically declare functions and classes : hp-performance-no-dynamic-cls-func : 1 : 11 : issue499.ts */
    return function Add() {
    };
  } else {
    /* HPAudit: Do not dynamically declare functions and classes : hp-performance-no-dynamic-cls-func : 1 : 13 : issue499.ts */
    return function Sub() {
    };
  }
}
