@Component
export default struct DebugInfo {
  @Prop debugWindow: boolean
  @Link vssMemory: number
  @Link pssMemory: number
  @Link sharedMemory: number
  @Link privateMemory: number
  build() {
    Stack({ 
      alignContent: Alignment.Top
    }) {
      Text($r('app.string.navigation_information'))
        .fontSize(25)
        .width('80%')
        .lineHeight(40)
        .margin({ 
          top: '5%'
        })
        .fontWeight(FontWeight.Bold)
      if (this.debugWindow) {
        Column() {
          this.showDebug($r('app.string.vss'), this.vssMemory)
          this.showDebug($r('app.string.pss'), this.pssMemory)
          this.showDebug($r('app.string.sharedDirty'), this.sharedMemory)
          this.showDebug($r('app.string.privateDirty'), this.privateMemory)
        }
          .id('debugInfo')
          .width('85%')
          .height('25%')
          .margin({ 
            top: '35%'
          })
          .padding({ 
            bottom: '5%',
            top: '5%'
          })
          .backgroundColor(Color.White)
          .border({ 
            width: 2,
            radius: 10
          })
      }
    }
      .width('100%')
      .height('100%')
  }
  @Builder private showDebug(title: Resource, data: number) {
    Row() {
      Text(title)
        .id('title')
        .fontSize(20)
        .width('60%')
        .margin({ 
          left: 6
        })
        .textAlign(TextAlign.End)
      Text(`${data}KB`)
        .id('storageSize')
        .fontSize(20)
    }
      .width('100%')
      .margin({ 
        top: 10
      })
  }
}
