/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue363.ts */
"use strict";
/**
 * in any class
 */
class MyClass {
  
  /* Member semicolon case */
  /* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 6 : issue363.ts */
  private max = 20;
  private min = 10;
  /* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 7 : issue363.ts */
  public age = 0;
  public name = '';
  /* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 9 : issue363.ts */
  foo(): void {
    
    /* Declaration comma case */
    /* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 11 : issue363.ts */
    /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 11 : issue363.ts */
    const a;
    /* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 11 : issue363.ts */
    let b;
    let c = "test";
    /* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 12 : issue363.ts */
    let d = 1;
    /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 12 : issue363.ts */
    /* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 12 : issue363.ts */
    const E = 2;

    /* Declaration semicolon case */
    /* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 15 : issue363.ts */
    let c = "semis";
    /* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 15 : issue363.ts */
    let f = 1;
    let g = 2;

    /* Assignment comma case */
    /* HPAudit: Do not assign multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 18 : issue363.ts */
    this.max = 50;
    /* HPAudit: Do not assign multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 19 : issue363.ts */
    this.min = 20;
    /* HPAudit: Do not assign multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 19 : issue363.ts */
    this.age = 20;
    this.name = 'bob'
    /* HPAudit: Do not assign multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 19 : issue363.ts */
    c = "hi";
    d = 42;

    /* Assignment semicolon case */
    /* HPAudit: Do not assign multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 22 : issue363.ts */
    this.max = 40;
    /* HPAudit: Do not assign multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 22 : issue363.ts */
    this.min = 20;
    /* HPAudit: Do not assign multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 22 : issue363.ts */
    this.age = 20;
    this.name = 'bob';
    /* HPAudit: Do not assign multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 23 : issue363.ts */
    b = "42";
    /* HPAudit: Do not assign multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 23 : issue363.ts */
    f = 42;
    g = 77
  }
}

/* Declaration comma case */
/* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 28 : issue363.ts */
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 28 : issue363.ts */
const a;
/* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 28 : issue363.ts */
let b;
let c = "test";
/* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 29 : issue363.ts */
let d = 1;
/* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 29 : issue363.ts */
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 29 : issue363.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 29 : issue363.ts */
const E = 2;
let myObj = new(MyClass);

/* Declaration semicolon case */
/* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 32 : issue363.ts */
let f = 1;
let g = 2;

/* Assignment comma case */
/* HPAudit: Do not assign multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 35 : issue363.ts */
myObj.age = 20;
/* HPAudit: Do not assign multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 35 : issue363.ts */
myObj.name = 'bob';
/* HPAudit: Do not assign multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 35 : issue363.ts */
c = "hi";
d = 42;

/* Assignment semicolon case */
/* HPAudit: Do not assign multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 38 : issue363.ts */
myObj.age = 20;
/* HPAudit: Do not assign multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 38 : issue363.ts */
myObj.name = 'bob';
/* HPAudit: Do not assign multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 38 : issue363.ts */
b = "42";
/* HPAudit: Do not assign multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 38 : issue363.ts */
f = 42;
g = 77
