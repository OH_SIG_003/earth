// Modifying indexOf() method of Array object.
Array.prototype.indexOf = function() {
  return -1;
}

// Adding a method to Array prototype.
Array.prototype.changeFirst = function() {
  this[0] = -1;
}

const arr = [1, 1, 1, 1, 1, 2, 1, 1, 1];
console.log(arr.indexOf(2)); //Output -1
arr.changeFirst();
console.log(arr);
