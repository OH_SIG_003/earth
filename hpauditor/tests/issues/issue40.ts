// An array should only be used store an ordered (indexed) set of data.
// Initializing an array
const myHash: string[] = [];

// Adding key-value pairs to array
myHash['key1'] = 'val1';    // flag this!
myHash['key2'] = 'val2';    // flag this!
// Adding a string as first element of array
myHash[0] = '222';

for (const key in myHash) {
  // The value of key is 0 key1 key2
  console.log(key);
}

console.log(myHash);
console.log('Length of array: ', myHash.length); //The array length is 1.

// Make sure that we don't flag objects as arrays
class HashClass {
    value: number = 0;
}

const yourHash : HashClass = { value: 42 };
yourHash['value'] = 333;    // do not flag this!

console.log(yourHash);
