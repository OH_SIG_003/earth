enum CallState {
  CALL_ACTIVE, //通话中*
  CALL_HOLDING, //通话保持
  CALL_DIALING, //拨号开始
  CALL_ALERTING, //正在呼出
  CALL_INCOMING, //来电*
  CALL_WAITING, //第三方来电*
  CALL_DISCONNECTED, //挂断完成 *
  CALL_DISCONNECTING, //正在挂断
  CALL_IDLE //空闲
}
