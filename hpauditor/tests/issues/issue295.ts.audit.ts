/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue295.ts */
"use strict";
/* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 1 : issue295.ts */
let x;
/* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 1 : issue295.ts */
let y;
/* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 1 : issue295.ts */
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 1 : issue295.ts */
const w;
/* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 1 : issue295.ts */
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 1 : issue295.ts */
const cw;
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 1 : issue295.ts */
const ch;
while (x * w < cw) {
  y = 0;
  while ((y + 1) * w < ch) {
    /* HPAudit: Use strict equality operators : hp-specs-strict-equality-ops : 1 : 5 : issue295.ts */
    if ((x + y) % 2 !== 0) {
      console.log(x, y);
    }
    y++
  }
  x++
}
