// This is an incorrect example because number of arguments and argument types
// do not match parameters for the function.
function add(a: number, b: number): number {
  return a + b;
}

add(1, 2, 3);
add(1);
add("hello", "world");
