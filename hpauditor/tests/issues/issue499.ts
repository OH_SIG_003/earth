function foo(f: boolean) {
  if (f) {
    return class Add{};
  } else {
    return class Sub{};
  }
}

function bar(b: boolean) {
  if (b) {
    return function Add(){};
  } else {
    return function Sub(){};
  }
}
