//TimeManager.ts
export const TIME_CHANGE_EVENT = 'Time_Change_Event';
export type TimeEventArgs = {
  date: Date;
  timeFormat: boolean;
};

const TAG = 'TimeManager';
const TIME_FORMAT_KEY = settings.date.TIME_FORMAT;
const log: LogHelper = LogHelper.getLogHelper(LogDomain.SYS_UI, TAG);

const TIME_SUBSCRIBE_INFO = {
  events: [
    commonEvent.Support.COMMON_EVENT_TIME_CHANGED, // 时间被设置
    commonEvent.Support.COMMON_EVENT_TIMEZONE_CHANGED, // 时区变化
    commonEvent.Support.COMMON_EVENT_TIME_TICK, // 当前时间变化
  ],
};
//...
let sTimeManager = SingletonHelper.getInstance(TimeManager, TAG);

export default sTimeManager as TimeManager;
