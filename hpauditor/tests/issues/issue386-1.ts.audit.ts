/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue386-1.ts */
"use strict";
function foo(): number {
  /* HPAudit: Do not assign variables in control conditions : hp-specs-no-vars-control-condition-expns : 1 : 2 : issue386-1.ts */
  for (let left = 22; 11 * 2; left++) {
    left = 11 * 2;
    return left
  }
  return 0;
}
