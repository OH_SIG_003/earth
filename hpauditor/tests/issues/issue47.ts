// Concatenates arguments.
function concatenateAll() {
  // As arguments is a class array, the join method cannot be used directly.
  // You need to convert arguments to a real array first.
  const args = Array.prototype.slice.call(arguments);
  return args.join('');
}

// Returns sum of arguments.
function getSum(a, b, c) {
  // Other ways to convert arguments to an array.
  const numbers1 = [].slice.call(arguments);
  const numbers2 = Array.from(arguments);
  const numbers3 = [...arguments];

  return numbers1.reduce((sum, num) => sum + num, 0);
}

console.log(concatenateAll('h', 'e', 'l', 'l', 'o'));
console.log(getSum(1, 2, 3));
