/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue55.ts */
"use strict";
// Force null and undefined as independent type annotations
// (Do not assign null or undefined unless type includes them)
let userName: string;
userName = 'hello';
/* HPAudit: Do not assign null or undefined to other types : hp-specs-null-undefined-independent-types : 1 : 5 : issue55.ts */
userName = undefined;
let num: number;
num = 17;
/* HPAudit: Do not assign null or undefined to other types : hp-specs-null-undefined-independent-types : 1 : 9 : issue55.ts */
num = null;
