/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue54.ts */
"use strict";
async function f3(): Promise<void> {
  /* HPAudit: Do not await non-thenables : hp-specs-no-await-non-thenables : 1 : 2 : issue54.ts */
  const a: number = await 20;
  /* HPAudit: Do not await non-thenables : hp-specs-no-await-non-thenables : 1 : 3 : issue54.ts */
  const b: string = await 'A';
  /* HPAudit: Do not await non-thenables : hp-specs-no-await-non-thenables : 1 : 4 : issue54.ts */
  const c: null = await null;
  /* HPAudit: Do not await non-thenables : hp-specs-no-await-non-thenables : 1 : 5 : issue54.ts */
  const d: undefined = await undefined;
  console.log(a);
  console.log(b);
  console.log(c);
  console.log(d);
  /* HPAudit: Do not await non-thenables : hp-specs-no-await-non-thenables : 1 : 10 : issue54.ts */
  a = await 20;
  /* HPAudit: Do not await non-thenables : hp-specs-no-await-non-thenables : 1 : 11 : issue54.ts */
  b = await 'A';
  /* HPAudit: Do not await non-thenables : hp-specs-no-await-non-thenables : 1 : 12 : issue54.ts */
  c = await null;
  /* HPAudit: Do not await non-thenables : hp-specs-no-await-non-thenables : 1 : 13 : issue54.ts */
  d = await undefined;
  console.log(a);
  console.log(b);
  console.log(c);
  console.log(d);
  /* HPAudit: Do not await non-thenables : hp-specs-no-await-non-thenables : 1 : 18 : issue54.ts */
  const e = await a;
  /* HPAudit: Do not await non-thenables : hp-specs-no-await-non-thenables : 1 : 19 : issue54.ts */
  const f = await b;
  /* HPAudit: Do not await non-thenables : hp-specs-no-await-non-thenables : 1 : 20 : issue54.ts */
  const g = await c;
  /* HPAudit: Do not await non-thenables : hp-specs-no-await-non-thenables : 1 : 21 : issue54.ts */
  const h = await d;
  console.log(e);
  console.log(f);
  console.log(g);
  console.log(h);
}
f3();
console.log(30);
// output

// 30

// 20

// A

// null
