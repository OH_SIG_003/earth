console.log(eval('{ a: 2 }')); // outputs 2
console.log(eval('let value = 1 + 1;')); // outputs undefined

async function foo(): Promise<number> {
  const baz: any = await eval('let a=88; console.log(a*a);');
  return baz;
}

foo()
