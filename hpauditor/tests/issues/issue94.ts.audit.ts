/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue94.ts */
"use strict";
// ArkTS formatting

// No space before ()
function f(): void {
  // f()
}
f(); // f()


// No space before []
let a: number[] = [1, 2, 3]; // number[]
a[1] = 42; // a[1]


// No space after :
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 13 : issue94.ts */
const X: number = 1; // x:
class C {
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 15 : issue94.ts */
  y: 1; // y:
}

// Space after { and before } in object literals
const o: { 
    a: number,
    b: number
  } = { 
    a: 1,
    b: 2
  };
