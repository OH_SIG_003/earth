/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue428.ts */
"use strict";
/* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 1 : issue428.ts */
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 1 : issue428.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 1 : issue428.ts */
const IS_LAND = false;
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 1 : issue428.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 1 : issue428.ts */
const ISC = false;
