// The type cannot be determined at compile time. It may be an object literal or another class Person.
type Person = {
  name: string;
  age: number;
};
 
function greet(person: Person) {
  return "Hello " + person.name;
}

// The type mode is not recommended, because it can be used in the following two modes, resulting in failure to determine the type at compile time.
// Invocation mode 1
class O1 {
  name: string = "";
  age: number = 0;
}
let objectliteral: O1 = {name : "zhangsan", age: 20 };
greet(objectliteral);

// Invocation mode 2
class Person {
  name: string = "zhangsan";
  age: number = 20;
}
let person = new Person();
greet(person);
