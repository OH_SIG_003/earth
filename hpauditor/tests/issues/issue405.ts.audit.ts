/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue405.ts */
"use strict";
//WifiController.ets
const TAG = 'Model.WifiController';
export class WifiController extends BaseController<WifiData> {
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 5 : issue405.ts */
  private currentWifiStatus: boolean = false;
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 6 : issue405.ts */
  private timerId: number = 0;
  mWifiData: WifiData = { 
      ... new WifiData()
    };
  constructor() {
    super();
    this.onStart();
  }
  //...
}
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 19 : issue405.ts */
const sWifiController = SingletonHelper.getInstance(WifiController, TAG);
export default sWifiController as WifiController;
