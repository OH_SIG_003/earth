/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue70.ts */
"use strict";
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 1 : issue70.ts */
const MAX_USER_COUNT: number = 10000;
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 2 : issue70.ts */
const MIN_USER_COUNT: number = 10;
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 3 : issue70.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 3 : issue70.ts */
const OVER_LIMIT: boolean = false;
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 4 : issue70.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 4 : issue70.ts */
const UNDER_LIMIT: boolean = true;
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 5 : issue70.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 5 : issue70.ts */
const USER_COUNT: number = 7;

// The line below exceeds line length limit.
if ((typeof USER_COUNT === "number") && (USER_COUNT > MAX_USER_COUNT && OVER_LIMIT || USER_COUNT < MIN_USER_COUNT && UNDER_LIMIT)) {
  console.log('The number of users need to be managed!');
}
