/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue68.ts */
"use strict";
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 1 : issue68.ts */
const num: number = - 1;
if (num < 0) {
  
  // Line below is at 152 characters.
  console.log(`${num} has been entered. A negative number has been detected. Negative numbers are not supported, please enter a non-negative number.`);
  console.log('An invalid number has been entered. A negative number has been detected. Negative numbers are not supported, please enter a non-negative number.');
  console.log("An invalid number has been entered. A negative number has been detected. Negative numbers are not supported, please enter a non-negative number.");
} else {
  console.log(`${num} has been entered. This number works!`);
}
