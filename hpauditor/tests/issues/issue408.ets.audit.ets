//StatusBarComponent
const TAG = "StatusBarComponent";

/**
 * 状态栏左右容器
 */
@Component
struct StatusBarGroup {
  
  /**
   * item区分左右排列方向
   */
  private static readonly POSITION_ITEM_DIRECTION: Map<string, Direction> = new Map([[PluginPosition.POSITION_LEFT, Direction.Auto], [PluginPosition.POSITION_RIGHT, Direction.Rtl]]);
  @Builder loadCapsulePluginInfos() {
    Row() {
      if (! ArrayUtils.isEmpty(this.dynamicPluginInfos)) {
        ForEach(this.dynamicPluginInfos, (pluginInfo: PluginInfo) => {
          CapsuleComponent({ 
            pluginInfo: pluginInfo,
            groupStyle: this.groupStyle,
            sortEvent: this.sortEvent.bind(this),
            isPcStatus: this.hasBackground()
          }) {
            PluginRootComponent({ 
              // 根组件唯一标示
              rootParentId: this.getRootId(),
              //
              pluginInfo: pluginInfo,
              //
              pluginStyle: this.getCommonPluginStyle(pluginInfo),
              //
              bgListenerManager: this.sbVm,
              //
              localIconComponent: this.localIconComponent,
              clickEvent: this.onItemClick.bind(this, pluginInfo),
              //
              rightClickEvent: this.onRightClick.bind(this, pluginInfo),
              hoverEvent: this.onItemHover.bind(this, pluginInfo),
              touchEvent: this.onItemTouch.bind(this, pluginInfo),
              longClickEvent: this.onItemLongClick.bind(this, pluginInfo),
              //
              doubleClickEvent: this.onItemDoubleClick.bind(this, pluginInfo),
            })
              .onAreaChange(this.onItemAreaChange.bind(this, pluginInfo))
          }
        }, (pluginInfo: PluginInfo) => pluginInfo?.pluginParseInfo?.pluginSlot)
      }
    }
      .direction(StatusBarGroup.POSITION_ITEM_DIRECTION.get(this.groupPosition))
      .id(`${TAG}_Row_loadCapsulePluginInfos`)
  }
}
