/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue51.ts */
"use strict";
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 1 : issue51.ts */
const CONT_NAME: string = 'name';
/* HPAudit: Do not use indexed containers as maps : hp-performance-proper-data-structures : 1 : 3 : issue51.ts */
let container: { 
    [key: string]: string
  } = { 
    aaa: 'bbb',
    ccc: 'ddd',
    [CONT_NAME]: 'container',
    7: '8',
  }

// Can be replaced with the constant equivalents, such as container.aaa
/* HPAudit: Do not delete properties of an object : hp-performance-no-delete-objs-props : 1 : 11 : issue51.ts */
/* HPAudit: Do not use indexed containers as maps : hp-performance-proper-data-structures : 1 : 11 : issue51.ts */
container['aaa'] = null;

// Dynamic, difficult-to-reason-about lookups
/* HPAudit: Do not delete properties of an object : hp-performance-no-delete-objs-props : 1 : 14 : issue51.ts */
/* HPAudit: Do not use indexed containers as maps : hp-performance-proper-data-structures : 1 : 14 : issue51.ts */
container[CONT_NAME] = null;
console.log(container);
