
# Usage: regression.sh [-comment] [other txl options]

# Clean up old results
/bin/rm -f *.ts.audit.ts *.ets.audit.ets

# Run each issue
for i in issue*.ts
do
    echo "=== $i ==="
    txl $1 $2 $3 $4 -q $i ../../txl/tsaudit.txl >& $i.audit.ts
    diff -w $i.audit.ts expected/$i.audit.ts
done
for i in issue*.ets
do
    echo "=== $i ==="
    txl $1 $2 $3 $4 -q $i ../../txl/tsaudit.txl >& $i.audit.ets
    diff -w $i.audit.ets expected/$i.audit.ets
done
echo "=== ==="
