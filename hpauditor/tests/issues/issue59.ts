const x: any = 1;
const y: any = true;

console.log(x + y);

const age: any = 'seventeen';

function greet(): any {
  return 'Hi!';
}

function doesNothing(param: any): void {}

function doesNothing2(param: Array<any>): void {}

function sayRandom(num: number): any {
  if (num > 10) {
    return 'Hello!';
  } else if (num > 5) {
    return num;
  } else {
    return true;
  }
}
