class FilePicker {
  public static async saveFile(dirPath: string): Promise<void> {
    if (isSaveFileSuccess) {
      let bundleName = storage.get<string>('bundleName');
      for (let i = 0; i < savedFileUris.length; i++) {
        await FilePickerUtil.grantUriPermission(savedFileUris[i], bundleName,
          wantConstant.Flags.FLAG_AUTH_READ_URI_PERMISSION |
          wantConstant.Flags.FLAG_AUTH_WRITE_URI_PERMISSION |
            // @ts-ignore
          wantConstant.Flags.FLAG_AUTH_PERSISTABLE_URI_PERMISSION);
      }
      let want = {
        parameters: {
          'pick_path_return': savedFileUris
        }
      };
      this.returnAbilityResult(want, 0);
    }
  }
}
