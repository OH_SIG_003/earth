function bar(): Promise<number> {
  return Promise.resolve(1);
}

async function foo(): Promise<number> {
  return await bar();
}

async function foo2(): Promise<number> {
  const baz = await bar();
  return baz;
}

async function foo3(): Promise<number> {
  try {
    return await bar();
  }
  catch (error) {
  }
}
