// Use dot notation to access object properties
class C {
  objName: string;
  country: string;
  age: number;
  colour: string;
}
const obj : C = {
  objName: 'obj',
  country: 'CA',
  age: 11,
  colour: 'red'
}

// But not arrays
type A = number[];
const array : A = [1,2,3];

// Using square brackets [] to access object properties (wrong)
const objName = obj['objName'];
const country = obj['country'];
const age = obj['age'];

console.log(objName);
console.log(country);
console.log(age);

// Using square brackets [] to access array elements (right)
const a1 = array[1];
const aa1 = array[a1]; 
console.log(a1);
console.log(aa1);
