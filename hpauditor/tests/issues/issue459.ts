import type rpc from '@ohos.rpc';
import type ServiceExtensionContext from 'application/ServiceExtensionContext';
import { Log } from '@ohos/common';
import { obtainLocalEvent } from '@ohos/common/src/main/ets/framework/utils/EventUtil';
import { sEventManager } from '@ohos/common/src/main/ets/framework/utils/EventManager';
import { SysDialogZOrder } from '../common/Constants';

const TAG = 'Dialog-ServiceExtensionAbility2';
const GT_KEY = TAG + 'SystemDialogController';

export const SCB_SYSTEM_DIALOG_LIST_EVENT = 'SCB_SYSTEM_DIALOG_LIST_EVENT';

type Tid = string;

const PARAMETER_EXTENSION_KEY = 'ability.want.params.uiExtensionType';

export interface IParameters {
  [PARAMETER_EXTENSION_KEY]: string
  sysDialogZOrder?: SysDialogZOrder
  [key: string]: Object
}

export interface IWant {
  bundleName?: string,
  abilityName?: string,
  parameters?: IParameters,
}
