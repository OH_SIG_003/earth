export default function * generatorFunc () : Generator <number, any, any> {
    console.log ("1. code before the first yield");
    yield 100;
    console.log ("2. code before the second yield");
    yield 200;
}
const generator : Generator <number, any, any> = generatorFunc ();
console.log (generator.next ());
