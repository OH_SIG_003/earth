#!/bin/bash
# Usage: regression.sh [-comment] [other txl options]
for i in import*.js export*.js
do
    echo "=== $i ==="
    txl $1 $2 $3 $4 -q $i js2ts.txl > $i.ts
    diff $i.ts expected/$i.ts
done
echo "=== ==="
