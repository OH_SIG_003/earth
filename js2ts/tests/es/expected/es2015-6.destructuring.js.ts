type JSDynamicObject = any;
const user : JSDynamicObject = {
    firstName : 'John',
    lastName : 'Kary'
};
const {firstName, lastName} : any = user;
console.log (firstName, lastName);
const [one, two, three] : any = ['one', 'two', 'three'];
console.log (one, two, three);
