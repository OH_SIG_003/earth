let mySet : Set <any> = new Set <any> ();
mySet.add (1);
mySet.add (2);
mySet.add (2);
mySet.add ('some text here');
mySet.add ({
    one : 1,
    two : 2,
    three : 3
});
console.log (mySet);
console.log (mySet.size);
console.log (mySet.has (2));
