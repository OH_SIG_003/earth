function message (name : string) : any {
    return console.log ("Hello, " + name + "!");
}
message ("Sudheer");
function multiply (x : number, y : number) : number {
    return x * y;
}
console.log (multiply (2, 3));
function even (number_ : number) : void {
    if (number_ % 2) {
        console.log ("Even");
    } else {
        console.log ("Odd");
    }
}
even (5);
function divide (x : number, y : number) : number {
    if (y != 0) {
        return x / y;
    }
}
console.log (divide (100, 5));
function greet () : any {
    return console.log ('Hello World!');
}
greet ();
