type JSDynamicObject = any;
let vehicle : JSDynamicObject = {};
let vehicle1 : JSDynamicObject = {
    car : {
        name : 'ABC',
        speed : 90
    }
};
console.log (vehicle.car ?.name);
console.log (vehicle.car ?.speed);
console.log (vehicle1.car ?.name);
console.log (vehicle1.car ?.speed);
console.log (vehicle.car ?.name ?? "Unknown");
console.log (vehicle.car ?.speed ?? 90);
