const promise : Promise <any> = new Promise <any> (function (resolve : any, reject : any) : any {
    setTimeout (() : any => {
            return resolve (1);
        }, 1000);
});
promise.then (function (result : any) : number {
    console.log (result);
    return result * 2;
}).then (function (result : any) : number {
    console.log (result);
    return result * 3;
}).then (function (result : any) : number {
    console.log (result);
    return result * 4;
}).catch (function (error : any) : void {
    console.log (error);
});
