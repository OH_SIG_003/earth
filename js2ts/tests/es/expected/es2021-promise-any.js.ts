let promise1 : Promise <any> = new Promise <any> ((resolve : any) : number => {
        return setTimeout (resolve, 100, 'Resolves after 100ms');
    });
let promise2 : Promise <any> = new Promise <any> ((resolve : any) : number => {
        return setTimeout (resolve, 200, 'Resolves after 200ms');
    });
let promise3 : Promise <any> = new Promise <any> ((resolve : any, reject : any) : number => {
        return setTimeout (reject, 0);
    });
let promises : Promise <any> [] = [promise1, promise2, promise3];
Promise.any (promises).then ((value : any) : any => {
        return console.log (value);
    });
