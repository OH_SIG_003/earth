// Params include trailing comma
function func(a,b,) { // declaration
    console.log(a, b);
}
func(1,2,); // invocation
