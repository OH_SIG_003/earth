// Atomic operations
const sharedMemory = new SharedArrayBuffer(1024);
const sharedArray = new Uint8Array(sharedMemory);
sharedArray[0] = 10;

Atomics.add(sharedArray, 0, 20);
console.log(Atomics.load(sharedArray, 0)); // 30

Atomics.sub(sharedArray, 0, 10);
console.log(Atomics.load(sharedArray, 0)); // 20

Atomics.and(sharedArray, 0, 5);
console.log(Atomics.load(sharedArray, 0));  // 4

Atomics.or(sharedArray, 0, 1);
console.log(Atomics.load(sharedArray, 0));  // 5

Atomics.xor(sharedArray, 0, 1);
console.log(Atomics.load(sharedArray, 0)); // 4

Atomics.store(sharedArray, 0, 10); // 10

Atomics.compareExchange(sharedArray, 0, 5, 10);
console.log(Atomics.load(sharedArray, 0)); // 10

Atomics.exchange(sharedArray, 0, 10);
console.log(Atomics.load(sharedArray, 0)); //10

Atomics.isLockFree(1); // true

// waiting to be notified
const sharedMemory1 = new SharedArrayBuffer(1024);
const sharedArray1 = new Int32Array(sharedMemory1);

Atomics.wait(sharedArray1, 0, 10);
console.log(sharedArray1[0]); // 100

Atomics.store(sharedArray1, 0, 100);
Atomics.notify(sharedArray1, 0, 1);