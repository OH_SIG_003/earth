// Creating objects using Reflect.construct()
class User {
    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    get fullName() {
        return `${this.firstName} ${this.lastName}`;
    }
};

let args = ['John', 'Emma'];

let john = Reflect.construct(
    User,
    args
);

console.log(john instanceof User);
console.log(john.fullName); // John Doe

//Calling a function using Reflect.apply()
const max = Reflect.apply(Math.max, Math, [100, 200, 300]);
console.log(max);

