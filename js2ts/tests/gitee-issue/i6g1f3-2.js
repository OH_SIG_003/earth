var value1 = 123;
value1 ||= 1;
console.log(value1);

var value2 = 0n;
value2 ||= 1n;
console.log(value2);

class A {
}
var value3 = 1;
value3 ||= new A();
console.log(value3);

var obj = {};
Object.defineProperty(obj, "prop", {
    value: 2,
    writable: true,
    enumerable: true,
    configurable: true
});
console.log(obj);
obj.prop ||= 1;
console.log(obj);
