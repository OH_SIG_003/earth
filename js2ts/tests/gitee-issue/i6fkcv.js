let a = 123;
let b = 2;
console.log(a ** b);

let c = 2n;
let d = 1n;
console.log(c ** d);

let e = 10.123;
let f = 2.123;
console.log(e ** f);
