const collection = { one: 1, two: 2, three: 3, [Symbol.iterator]() {

        const values = Object.keys(this);

        let i = 0;

        return { next: () => {

                return { value: this[values[i++]], done: i > values.length };

            } };

    }

};

const iterator = collection[Symbol.iterator]();

console.log(iterator.next());

console.log(iterator.next());

console.log(iterator.next());

console.log(iterator.next());

for (const value of collection) {

    console.log(value);

}
