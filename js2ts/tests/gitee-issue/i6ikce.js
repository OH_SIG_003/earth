var reHex = /^#([0-9a-f]{3,8})$/;
var reI = "\\s*([+-]?\\d+)\\s*";
var reN = "\\s*([+-]?(?:\\d*\\.)?\\d+(?:[eE][+-]?\\d+)?)\\s*";
var reP = "\\s*([+-]?(?:\\d*\\.)?\\d+(?:[eE][+-]?\\d+)?)%\\s*";
var reHex = /^#([0-9a-f]{3,8})$/;
var reRgbaInteger = new RegExp("^rgba\\(".concat(reI, ",").concat(reI, ",").concat(reI, ",").concat(reN, "\\)$"));
var reHslPercent = new RegExp("^hsl\\(".concat(reN, ",").concat(reP, ",").concat(reP, "\\)$"));
function color(format) {
    var m;
    format = (format + "").trim().toLowerCase();
    var l;
    return (m = reHex.exec(format)) ? (l = m[1].length, m = parseInt(m[1], 16), l === 6 ? (m = reRgbaInteger.exec(format)) ? rgba(m[1], m[2], m[3], m[4]) : null : null) : null;
}
function rgba(r, g, b, a) {
    if (a <= 0)
        r = g = b = NaN;
    return new Rgb(r, g, b, a);
}
var Rgb = /** @class */ (function () {
    function Rgb(r, g, b, opacity) {
        this.r = (+r);
        this.g = (+g);
        this.b = (+b);
        this.opacity = (+opacity);
    }
    return Rgb;
}());
console.log(color("#abcedf"));
