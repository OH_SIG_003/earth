// Copyright 2020 Salesforce.com, Inc. All rights reserved.
// This code is governed by the BSD license found in the LICENSE file.
/*---
esid: prod-OptionalExpression
description: >
  Productions for ?. [Expression]
info: |
  OptionalChain:
    ?.[ Expression ]
features: [optional-chaining]
---*/

function sameValue(a,b,c){};
const $ = 'x';
const arr = [39, 42];

arr.true = 'prop';
arr[1.1] = 'other prop';
const obj = {
  a: 'hello',
  undefined: 40,
  $: 0,
  NaN: 41,
  null: 42,
  x: 43,
  true: 44
};

console.log(arr?.[0]);
sameValue(arr?.[0], 39, '[0]');
console.log(arr?.[0,1]);
sameValue(arr?.[0, 1], 42, '[0, 1]');
sameValue(arr?.[1], 42, '[1]');
sameValue(arr?.[1, 0], 39, '[1, 0]');
sameValue(arr?.[{}, NaN, undefined, 2, 0, 10 / 10], 42, '[{}, NaN, undefined, 2, 0, 10 / 10]');
sameValue(arr?.[true], 'prop', '[true]');
sameValue(arr?.[1.1], 'other prop', '[1.1]');

sameValue(obj?.[undefined], 40, '[undefined]');
sameValue(obj?.[NaN], 41, '[NaN]');
sameValue(obj?.[null], 42, '[null]');
sameValue(obj?.['$'], 0, '["$"]');
sameValue(obj?.[$], 43, '[$]');
sameValue(obj?.[true], 44, '[true]');	

