// Copyright 2009 the Sputnik authors.  All rights reserved.
// This code is governed by the BSD license found in the LICENSE file.

/*---
info: The depth of nested function calls reaches 32
es5id: 13.2.1_A1_T1
description: Creating function calls 32 elements depth
---*/

(function(){
    (function(){
        (function(){
            (function(){
                (function(){
                    (function(){
                        (function(){
                            (function(){
                                (function(){
                                    (function(){
                                        (function(){
                                            (function(){
                                                (function(){
                                                    (function(){
                                                        (function(){
                                                            (function(){
                                                                (function(){
                                                                    (function(){
                                                                        (function(){
                                                                            (function(){
                                                                                (function(){
// 22                                                                                  (function(){
// 23                                                                                        (function(){
// 24                                                                                            (function(){
// 25                                                                                                (function(){
// 26                                                                                                    (function(){
// 27                                                                                                       (function(){
// 28                                                                                                           (function(){
// 29                                                                                                               (function(){
// 30                                                                                                                   (function(){
// 31                                                                                                                       (function(){
// level 32                                                                                                                   (function(){})()
// 31                                                                                                                       })()
// 30                                                                                                                   })()
// 29                                                                                                               })()
// 28                                                                                                           })()
// 27                                                                                                        })()
// 26                                                                                                    })()
// 25                                                                                                })()
// 24                                                                                            })()
// 23                                                                                        })()
// 22                                                                                    })()
                                                                                })()
                                                                            })()
                                                                        })()
                                                                    })()
                                                                })()
                                                            })()
                                                        })()
                                                    })()
                                                })()
                                            })()
                                        })()
                                    })()
                                })()
                            })()
                        })()
                    })()
                })()
            })()
        })()
    })()
})()

