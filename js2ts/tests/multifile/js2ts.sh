#!/bin/sh
# Process a Javascript main file and its imports
# Jim Cordy, June 2023

ulimit -s hard

# Check argument
if [[ ! "$1" =~ (.*\.js) ]]; then 
    echo "Usage:  js2ts.sh mainfile.js"
    echo "  (where "mainfile.js" is the main Javascript file of the program)"
    exit 99
else
    main="$1"
    main="${main%.js}"
fi

echo "Processing "$main".js and its imported .js files"

# Process using js2ts
echo ""
echo "Running js2ts ..."
txl -q "$main".js js2ts.txl -s 400 -d NOUNEXPAND > "$main".js.ts 

# Split the result into TS files
echo ""
echo "Splitting result into TS files ..."
txl -q "$main".js.ts js2ts-split.txl > "$main".ts

echo ""
echo "Done - Results in "$main".ts and its imported .ts files"
echo ""
echo "*** WARNING *** Be careful - running tsc on the results will overwrite the original JS files"
echo ""
