var obj = {
  get [Symbol.iterator]() {
    console.log ('it should not get Symbol.iterator');
    return 99;
  },
  [Symbol.asyncIterator]() {
    return {
      next() {
        return 42;
      }
    };
  }
};

var C = class {
    callCount = 0;
    async *#gen() {
        this.callCount += 1;
        yield* obj;
        console.log ('abrupt completion closes iter');
    }
    get gen() { return this.#gen; }
}

const c = new C();
console.log(c.gen())

