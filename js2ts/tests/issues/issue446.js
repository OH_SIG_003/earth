function example(arg1, arg2) {
    if (typeof arg1 === 'function') {
      console.log('arg1 is a function');
    } else {
      console.log('arg1 is not a function');
    }
  }
  
  example('string');
  example(function() {});
