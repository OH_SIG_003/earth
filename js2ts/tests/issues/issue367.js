
// Test Intl.Collator
const collator = new Intl.Collator("en");
const collatorResult = ["apple", "banana", "Cherry"].sort(collator.compare);
console.log("Collator:", collatorResult);

// Test Intl.DateTimeFormat
const dateTimeFormat = new Intl.DateTimeFormat("en-US", {
  year: "numeric",
  month: "long",
  day: "numeric",
});
const formattedDate = dateTimeFormat.format(new Date());
console.log("DateTimeFormat:", formattedDate);

// Test Intl.ListFormat
const listFormat = new Intl.ListFormat("en-US", {
  style: "long",
  type: "conjunction",
});
const formattedList = listFormat.format(["apple", "banana", "cherry"]);
console.log("ListFormat:", formattedList);

// Test Intl.NumberFormat
const numberFormat = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
});
const formattedNumber = numberFormat.format(123456.789);
console.log("NumberFormat:", formattedNumber);

// Test Intl.PluralRules
const pluralRules = new Intl.PluralRules("en-US", { type: "ordinal" });
const formattedPlural = pluralRules.select(22);
console.log("PluralRules:", formattedPlural);

// Test Intl.RelativeTimeFormat
const relativeTimeFormat = new Intl.RelativeTimeFormat("en-US", {
  numeric: "auto",
});
const formattedRelativeTime = relativeTimeFormat.format(-1, "day");
console.log("RelativeTimeFormat:", formattedRelativeTime);

