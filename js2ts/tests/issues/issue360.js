var v1 = Reflect.apply(Math.floor, undefined, [1.75]);
console.log(v1);
// Expected output: 1

function func1(a, b, c) {
  this.sum = a + b + c;
}

const args = [1, 2, 3];
const object1 = new func1(1,2,3);
const object2 = Reflect.construct(func1, args);

console.log(object2.sum);
// Expected output: 6

console.log(object1.sum);
// Expected output: 6

const object3 = {};
var v2 = Reflect.defineProperty(object3, 'property1', { value: 42 });
if (v2) {
  console.log('property1 created!');
  // Expected output: "property1 created!"
} else {
  console.log('problem creating property1');
}

console.log(object3.property2);
// Expected output: 43

const object4 = {
  property1: 42
};

var v3 = Reflect.deleteProperty(object4, 'property1');

const array1 = [1, 2, 3, 4, 5];
Reflect.deleteProperty(array1, '3');

console.log(array1);
// Expected output: Array [1, 2, 3, undefined, 5]

const object5 = {
  x: 1,
  y: 2
};

var v4 = Reflect.get(object5, 'x');

console.log(v4);
// Expected output: 1

const array2 = ['zero', 'one'];

console.log(Reflect.get(array2, 1));
// Expected output: "one"

const object6 = {
  property1: 42
};

var v5 = Reflect.getOwnPropertyDescriptor(object6, 'property1');
console.log(v5.value);
// Expected output: 42

console.log(Reflect.getOwnPropertyDescriptor(object6, 'property2'));
// Expected output: undefined

console.log(Reflect.getOwnPropertyDescriptor(object6, 'property1').writable);
// Expected output: true

const object7 = {
  property1: 42
};

const proto1 = Reflect.getPrototypeOf(object7);

console.log(proto1);
// Expected output: Object {  }

console.log(Reflect.getPrototypeOf(proto1));
// Expected output: null

var v6 = Reflect.has(object6, 'property1');
console.log(v6);
// Expected output: true

console.log(Reflect.has(object1, 'property2'));
// Expected output: false

var v7 = Reflect.isExtensible(object3);
console.log(v7);
// Expected output: true

var v8 = Reflect.preventExtensions(object3);

console.log(v8);

console.log(Reflect.isExtensible(object3));
// Expected output: false

const object8 = Object.seal({});
console.log(Reflect.isExtensible(object8));
// Expected output: false

const object9 = {
  property1: 42,
  property2: 13
};

const array3 = [];

var v9 = Reflect.ownKeys(object9);
console.log(v9);
// Expected output: Array ["property1", "property2"]

console.log(Reflect.ownKeys(array3));
// Expected output: Array ["length"]
console.log(Reflect.setPrototypeOf(object1, Object.prototype));
// Expected output: true

console.log(Reflect.setPrototypeOf(object1, null));
// Expected output: true

var v10 = Reflect.set(object3, 'property1', 42);
console.log(object3.property1);
// Expected output: 42

const array4 = ['duck', 'duck', 'duck'];
v10 = Reflect.set(array1, 2, 'goose');

console.log(array4[2]);
// Expected output: "goose"
var v11 = Reflect.setPrototypeOf(object3, Object.prototype);
console.log(v11);
// Expected output: true
v11 = Reflect.setPrototypeOf(object3, null);
console.log(v11);
// Expected output: true
