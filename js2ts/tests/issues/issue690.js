"use strict";
class Mother {
    constructor(name, foot) {
        this.name = name;
        this.foot = foot;
    }
}
class Child extends Mother {
    constructor(name, foot, eye) {
        super(name, foot);
        this.eye = eye;
    }
}
let child = new Child("Panji Asmoro", 2, "Hitam");
console.log(child);
