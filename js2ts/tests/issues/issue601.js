// Copyright (C) 2015 the V8 project authors. All rights reserved.
// This code is governed by the BSD license found in the LICENSE file.

/*---
info: Correct interpretation of RUSSIAN ALPHABET
es6id: 11.6
description: Check RUSSIAN SMALL ALPHABET
---*/

function assert_sameValue (x,y) {
    console.log (x == y);
}

var а = 1;
assert_sameValue(а, 1);

var б = 1;
assert_sameValue(б, 1);

var в = 1;
assert_sameValue(в, 1);

var г = 1;
assert_sameValue(г, 1);

var д = 1;
assert_sameValue(д, 1);

var е = 1;
assert_sameValue(е, 1);

var ж = 1;
assert_sameValue(ж, 1);

var з = 1;
assert_sameValue(з, 1);

var и = 1;
assert_sameValue(и, 1);

var й = 1;
assert_sameValue(й, 1);

var к = 1;
assert_sameValue(к, 1);

var л = 1;
assert_sameValue(л, 1);

var м = 1;
assert_sameValue(м, 1);

var н = 1;
assert_sameValue(н, 1);

var о = 1;
assert_sameValue(о, 1);

var п = 1;
assert_sameValue(п, 1);

var р = 1;
assert_sameValue(р, 1);

var с = 1;
assert_sameValue(с, 1);

var т = 1;
assert_sameValue(т, 1);

var у = 1;
assert_sameValue(у, 1);

var ф = 1;
assert_sameValue(ф, 1);

var х = 1;
assert_sameValue(х, 1);

var ц = 1;
assert_sameValue(ц, 1);

var ч = 1;
assert_sameValue(ч, 1);

var ш = 1;
assert_sameValue(ш, 1);

var щ = 1;
assert_sameValue(щ, 1);

var ъ = 1;
assert_sameValue(ъ, 1);

var ы = 1;
assert_sameValue(ы, 1);

var ь = 1;
assert_sameValue(ь, 1);

var э = 1;
assert_sameValue(э, 1);

var ю = 1;
assert_sameValue(ю, 1);

var я = 1;
assert_sameValue(я, 1);

var ё = 1;
assert_sameValue(ё, 1);
