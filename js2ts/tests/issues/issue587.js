const d = {
    Ls: {
        en: { name: 'English' },
        fr: { name: 'French' },
        es: { name: 'Spanish' }
    }
};

function myFunction(args) {
    const isStrictWithLocale = args[3] === true;
    let pl = args[2];
    if (!isStrictWithLocale) [,,, pl] = args;
    console.log(d.Ls[pl]);
}

myFunction([,,,'fr']);
