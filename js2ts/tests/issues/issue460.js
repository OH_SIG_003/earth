function foo(obj) {
    if (typeof obj === 'string') {
        foo = function foo(obj) {
            return typeof obj
        }
    } else {
        foo = function foo(obj) {
            return "not string"
        }
    }
    return foo(obj)
}

console.log(foo("a"))
console.log(foo(1))
