let expression = "5 + 3";
let result : undefined = eval(expression);
console.log(result); // Output: 8

let complexExpression = "(2 * 3) + Math.sqrt(16) / 2";
let complexResult = eval(complexExpression);
console.log(complexResult); // Output: 8

let stringExpression = "'Hello, ' + 'World!'";
let stringResult = eval(stringExpression);
console.log(stringResult); // Output: "Hello, World!"

let assignmentExpression = "let x = 10; x * 2";
let assignmentResult = eval(assignmentExpression);
console.log(assignmentResult); // Output: 20

let functionExpression = "(function(a, b) { return a * b; })(3, 4)";
let functionResult = eval(functionExpression);
console.log(functionResult); // Output: 12
