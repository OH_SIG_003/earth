var a = 111;

{
    var a = "abc";  // hoisted to global variable declaration
    console.log (a);
}

console.log (a);

function foo1() {
    var a = 222;    // local variable declaration
    console.log (a);

    function bar() {
        return a;
    }

    return a;
}

console.log(foo1());

function foo2() {
    var a = 333;
    console.log (a);

    if (a == 333) {
        var a = "def";  // hoisted to local variable declaration
        console.log (a);
    }

    console.log (a);

    function bar() {
        return a;
    }

    return a;
}

console.log (foo2());
