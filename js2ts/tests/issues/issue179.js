const littleEndian = (() => {
  const buffer = new ArrayBuffer(2);
  var dataview =  new DataView(buffer);
  dataview.setInt16(0, 256, true);
  var ret = new Int16Array(buffer)[0] === 256;
  return ret;
})();
console.log(littleEndian);
