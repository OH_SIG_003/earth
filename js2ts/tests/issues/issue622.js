export default (function zoomRho(rho, rho2) {

   function zoom(p0) {
    var ux0 = p0[0], uy0 = p0[1], w0 = p0[2],
        dx = 0.1,
        dy = 0.2,
        i;

    if (dx < 0.9) {
      i = function(t) {
        return [
          ux0 + t * dx,
          uy0 + t * dy,
          w0 * Math.exp(rho * t)
        ];
      }
    }
    else {
      i = function() {
        var u = w0 / rho2  ;
        return [
          ux0 + u * dx,
          uy0 + u * dy,
          w0
        ];
      }
    }
    i.duration = 1000 * rho / Math.SQRT2;
    return i;
  }
  return zoom;
});
