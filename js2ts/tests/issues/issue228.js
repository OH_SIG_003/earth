// union formal parameters
function f1 (x) {
    x = x + 42;
    console.log (x);
}

f1(0);
f1("abc ");

function f2 (x,y) {
    if (x == 1) {
        return x + 41;
    } else {
        return y;
    }
}

console.log(f2(1,"def"));
console.log(f2("abc",42));

function f3 (x) {
    if (x == "abc") {
        console.log (x);
    }
    console.log (99);
}

x = 42;
x = "abc";
f3(x);

class C {
    f1 (x) {
        x = x + 42;
        console.log (x);
    }

    static f2 (x,y) {
        if (x == 1) {
            return x + 41;
        } else {
            return y;
        }
    }

    f3 (x) {
        if (x == "abc") {
            console.log (x);
        }
        console.log (99);
    }
}

o = new C();
o.f1(0);
o.f1("abc ");

console.log(C.f2(1,"def"));
console.log(C.f2("abc",42));

z = true;
z = 99;
z = "abc";
o.f3(x);
