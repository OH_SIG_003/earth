import {Basis} from "./issue537-2.js";

function Bundle(context, beta) {
  this._basis = new Basis(context);
  this._beta = beta;
}
