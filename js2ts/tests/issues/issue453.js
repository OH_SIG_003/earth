class Foo {
    constructor(c) {
        this.c = c
    }
}

console.log (new Foo("constructor property").c)

// dynamically add static property
Foo.s = "added static property"
console.log (Foo.s)

// dynamically add instance property - applies to all subsequent instances
Foo.prototype.i = "added instance property"
i = new Foo("constructor property")
console.log (i.i)

// dynamically add object property - applies to this object only!
o = new Foo("constructor property")
o.o = "added oject property"
console.log (o.o)
