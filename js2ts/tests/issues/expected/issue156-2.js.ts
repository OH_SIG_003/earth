var callCount : number = 0;
function f ( ... JSarguments : any []) : void {
    callCount += 1;
}
f (5, ... ["six", "seven", "eight"]);
console.log (callCount);
