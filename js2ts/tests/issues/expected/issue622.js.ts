export default function zoomRho (rho : any, rho2 : any) : any {
    function zoom (p0 : any []) : any {
        var ux0 : any = p0 [0];
        var uy0 : any = p0 [1];
        var w0 : any = p0 [2];
        var dx : number = 0.1;
        var dy : number = 0.2;
        var i ! : any;
        if (dx < 0.9) {
            i = function (t : any) : any [] {
                return [ux0 + t * dx, uy0 + t * dy, w0 * Math.exp (rho * t)];
            }
        } else {
            i = function () : any [] {
                var u : any = w0 / rho2;
                return [ux0 + u * dx, uy0 + u * dy, w0];
            }
        }
        i.duration = 1000 * rho / Math.SQRT2;
        return i;
    }
    return zoom;
};
