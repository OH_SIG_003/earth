export const foo : (x1 ? : boolean) => string = function (a : boolean = false) : string {
    return a === true ? 'true' : 'false';
}
console.log (foo ());
console.log (foo (true));
