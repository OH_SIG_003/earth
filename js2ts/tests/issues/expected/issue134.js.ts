type JSDynamicObject = any;
var obj : JSDynamicObject = {
    value : 123,
    name : "abc",
    foo () : number {
        return 42;
    }
};
console.log (obj);
