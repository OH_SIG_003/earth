type JSDynamicObject = any;
const testData : JSDynamicObject [] = [{
    tag : "cel-gaulish",
    options : {
        language : "fr",
        script : "Cyrl",
        region : "FR",
        numberingSystem : "latn",
    },
    canonical : "fr-Cyrl-FR-u-nu-latn",
}, {
    tag : "art-lojban",
    options : {
        language : "fr",
        script : "Cyrl",
        region : "ZZ",
        numberingSystem : "latn",
    },
    canonical : "fr-Cyrl-ZZ-u-nu-latn",
},];
function sameValue (a : string, b : any) : void {
    console.log (a);
    console.log (b);
}
for (const {tag, options, canonical} of testData) {
    const loc : Intl.Locale = new Intl.Locale (tag, options);
    sameValue (loc.toString (), canonical);
    for (const [name, value] of Object.entries (options)) {
        sameValue (loc [name], value);
    }
}
function MyAssertThrow (err : any, foo : any) : void {
}
MyAssertThrow (RangeError, () : Intl.Locale => {
        return new Intl.Locale ("i-default", {
            language : "fr",
            script : "Cyrl",
            region : "DE",
            numberingSystem : "latn"
        });
    });
MyAssertThrow (RangeError, () : Intl.Locale => {
        return new Intl.Locale ("en-gb-oed", {
            language : "fr",
            script : "Cyrl",
            region : "US",
            numberingSystem : "latn"
        });
    });
