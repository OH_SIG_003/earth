function disjoint (values : any, other : any) : boolean {
    const iterator : any = other [Symbol.iterator] ();
    const set : Set <any> = new Set <any> ();
    for (const v of values) {
        if (set.has (v)) return false;
        let value ! : any;
        let done ! : any;
        while ({
            value,
            done
        } = iterator.next ()) {
            if (done) break;
            if (Object.is (v, value)) return false;
            set.add (value);
        }
    }
    return true;
}
