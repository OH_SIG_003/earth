function isValidAlphaEscapeInAtom (s : string) : boolean {
    switch (s) {
        case "b" :
        case "B" :
        case "f" :
        case "n" :
        case "r" :
        case "t" :
        case "v" :
        case "d" :
        case "D" :
        case "s" :
        case "S" :
        case "w" :
        case "W" :
            return true;
        default :
            return false;
    }
}
function isValidAlphaEscapeInClass (s : string) : boolean {
    switch (s) {
        case "b" :
        case "f" :
        case "n" :
        case "r" :
        case "t" :
        case "v" :
        case "d" :
        case "D" :
        case "s" :
        case "S" :
        case "w" :
        case "W" :
            return true;
        default :
            return false;
    }
}
for (var cu : number = 0x41; cu <= 0x5a; ++ cu) {
    var s : string = String.fromCharCode (cu);
    if (! isValidAlphaEscapeInAtom (s)) {
        assert.throws (SyntaxError, function () : void {
            RegExp ("\\" + s, "u");
        }, "IdentityEscape in AtomEscape: '" + s + "'");
    }
}
for (var cu : number = 0x61; cu <= 0x7a; ++ cu) {
    var s : string = String.fromCharCode (cu);
    if (! isValidAlphaEscapeInAtom (s)) {
        assert.throws (SyntaxError, function () : void {
            RegExp ("\\" + s, "u");
        }, "IdentityEscape in AtomEscape: '" + s + "'");
    }
}
for (var cu : number = 0x41; cu <= 0x5a; ++ cu) {
    var s : string = String.fromCharCode (cu);
    if (! isValidAlphaEscapeInClass (s)) {
        assert.throws (SyntaxError, function () : void {
            RegExp ("[\\" + s + "]", "u");
        }, "IdentityEscape in ClassEscape: '" + s + "'");
    }
}
for (var cu : number = 0x61; cu <= 0x7a; ++ cu) {
    var s : string = String.fromCharCode (cu);
    if (! isValidAlphaEscapeInClass (s)) {
        assert.throws (SyntaxError, function () : void {
            RegExp ("[\\" + s + "]", "u");
        }, "IdentityEscape in ClassEscape: '" + s + "'");
    }
}
