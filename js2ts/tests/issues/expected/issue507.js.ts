type JSNoValue = any;
var epsilon : number = 0.0000001;
function intersect (x0 : number, y0 : number, x1 : number, y1 : number, x2 : number, y2 : number, x3 : number, y3 : number) : number [] | JSNoValue {
    var x10 : number = x1 - x0;
    var y10 : number = y1 - y0;
    var x32 : number = x3 - x2;
    var y32 : number = y3 - y2;
    var t : number = y32 * x10 - x32 * y10;
    if (t * t < epsilon) return;
    t = (x32 * (y0 - y2) - y32 * (x0 - x2)) / t;
    return [x0 + t * x10, y0 + t * y10];
}
var x01 : number = 0.009;
var y01 : number = 0.008;
var x10 : number = 0.09;
var y10 : number = 0.8;
var x11 : number = 0.9;
var y11 : number = 0.008;
var x00 : number = 0.07;
var y00 : number = 0.2;
var oc ! : number [];
if (oc = intersect (x01, y01, x00, y00, x11, y11, x10, y10)) {
    console.log ("Hello");
}
