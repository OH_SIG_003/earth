type JSDynamicObject = any;
const irregularGrandfathered : string [] = ["en-GB-oed", "i-ami", "i-bnn", "i-default", "i-enochian", "i-hak", "i-klingon", "i-lux", "i-mingo", "i-navajo", "i-pwn", "i-tao", "i-tay", "i-tsu", "sgn-BE-FR", "sgn-BE-NL", "sgn-CH-DE",];
for (const tag of irregularGrandfathered) {
    assert.throws (RangeError, () : Intl.Locale => {
            return new Intl.Locale (tag);
        });
}
const regularGrandfathered : JSDynamicObject [] = [{
    tag : "art-lojban",
    canonical : "jbo",
    maximized : "jbo-Latn-001",
}, {
    tag : "cel-gaulish",
    canonical : "xtg",
}, {
    tag : "zh-guoyu",
    canonical : "zh",
    maximized : "zh-Hans-CN",
}, {
    tag : "zh-hakka",
    canonical : "hak",
    maximized : "hak-Hans-CN",
}, {
    tag : "zh-xiang",
    canonical : "hsn",
    maximized : "hsn-Hans-CN",
},];
for (const {tag, canonical, maximized = canonical, minimized = canonical} of regularGrandfathered) {
    const loc : Intl.Locale = new Intl.Locale (tag);
    assert.sameValue (loc.toString (), canonical);
    assert.sameValue (loc.maximize ().toString (), maximized);
    assert.sameValue (loc.maximize ().maximize ().toString (), maximized);
    assert.sameValue (loc.minimize ().toString (), minimized);
    assert.sameValue (loc.minimize ().minimize ().toString (), minimized);
    assert.sameValue (loc.maximize ().minimize ().toString (), minimized);
    assert.sameValue (loc.minimize ().maximize ().toString (), maximized);
}
const regularGrandfatheredWithExtLang : string [] = ["no-bok", "no-nyn", "zh-min", "zh-min-nan",];
for (const tag of regularGrandfatheredWithExtLang) {
    assert.throws (RangeError, () : Intl.Locale => {
            return new Intl.Locale (tag);
        });
}
const extras : string [] = ["fonipa", "a-not-assigned", "u-attr", "u-co", "u-co-phonebk", "x-private",];
for (const {tag, canonical} of regularGrandfathered) {
    const priv : string = "-x-0";
    const tagMax : any = new Intl.Locale (canonical + priv).maximize ().toString ().slice (0, - priv.length);
    const tagMin : any = new Intl.Locale (canonical + priv).minimize ().toString ().slice (0, - priv.length);
    for (const extra of extras) {
        const loc : Intl.Locale = new Intl.Locale (tag + "-" + extra);
        let canonicalWithExtra : string = canonical + "-" + extra;
        let canonicalMax : string = tagMax + "-" + extra;
        let canonicalMin : string = tagMin + "-" + extra;
        if (/^[a-z0-9]{5,8}|[0-9][a-z0-9]{3}$/i.test (extra)) {
            function sorted (s : string) : string {
                return s.replace (/(-([a-z0-9]{5,8}|[0-9][a-z0-9]{3}))+$/i, (m : any) : any => {
                        return m.split ("-").sort ().join ("-");
                    });
            };
            canonicalWithExtra = sorted (canonicalWithExtra);
            canonicalMax = sorted (canonicalMax);
            canonicalMin = sorted (canonicalMin);
        }
        assert.sameValue (loc.toString (), canonicalWithExtra);
        assert.sameValue (loc.maximize ().toString (), canonicalMax);
        assert.sameValue (loc.maximize ().maximize ().toString (), canonicalMax);
        assert.sameValue (loc.minimize ().toString (), canonicalMin);
        assert.sameValue (loc.minimize ().minimize ().toString (), canonicalMin);
        assert.sameValue (loc.maximize ().minimize ().toString (), canonicalMin);
        assert.sameValue (loc.minimize ().maximize ().toString (), canonicalMax);
    }
}
