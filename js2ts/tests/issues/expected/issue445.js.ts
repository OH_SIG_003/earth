type JSDynamicObject = any;
function convertObject (param : JSDynamicObject) : any {
    let list : any [] = [];
    if (Object.prototype.toString.call (param) === '[object Object]') {
        let keys : string [] = Object.keys (param);
        keys.forEach ((key : any) : any => {
                list.push ({
                    name : key,
                });
            });
        return list;
    } else {
        switch (Object.prototype.toString.call (param)) {
            case 'regexp' :
                return param.toString ();
            case 'function' :
                return ' ƒ() {...}';
            default :
                return `"${param.toString()}"`;
        }
    }
};
const obj : JSDynamicObject = {
    a : 1,
    b : 2,
    c : 3
};
const result : any = convertObject (obj);
console.log (result);
