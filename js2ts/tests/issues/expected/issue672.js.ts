type JSDynamicObject = any;
function mutation (x : any, y : any) : void {
}
function relativeTimeWithMutation (number_ : any, withoutSuffix : any, key : any) : string {
    const format : JSDynamicObject = {
        mm : 'munutenn',
        MM : 'miz',
        dd : 'devezh'
    };
    return `${number_} ${mutation(format[key], number_)}`;
}
