async function * foo () : AsyncGenerator <any, any, any> {
    yield await Promise.resolve ('a');
    yield await Promise.resolve ('b');
    yield await Promise.resolve ('c');
}
let str : string = '';
async function generate1 () : Promise <any> {
    for await (const val of foo ()) {
        str = str + val;
    }
    console.log (str);
}
generate1 ();
