function func1 ( ... JSarguments : number []) : void {
    let a : number = JSarguments [0];
    let b : number = JSarguments [1];
    let c : number = JSarguments [2];
    console.log (JSarguments [0]);
    console.log (JSarguments [1]);
    console.log (JSarguments [2]);
}
func1 (1, 2, 3);
function sum ( ... args : number []) : number {
    let sum : number = 0;
    for (let arg of args) sum += arg;
    return sum;
}
let x : number = sum (4, 9, 16, 25, 29, 100, 66, 77);
