type JSDynamicObject = any;
function arc () : JSDynamicObject {
    var buffer : number = 0;
}
arc.centroid = function () : number [] {
    var r : number = 2;
    var a : number = 4;
    return [Math.cos (a) * r, Math.sin (a) * r];
}
;
