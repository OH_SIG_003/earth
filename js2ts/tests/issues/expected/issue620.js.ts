type JSDynamicObject = any;
function range (i : number, j : number) : any [] {
    return Array.from ({
        length : j - i
    }, (_ : any, k : any) : any => {
            return i + k;
        });
}
function chord (this : JSDynamicObject, directed : any, transpose : any) : any {
    function chord (this : JSDynamicObject, matrix : any [] [] | Float64Array) : any {
        var n : number = matrix.length;
        var groupIndex : any [] = range (0, n);
        var chords : any [] = [];
        var groups : any [] = [];
        matrix = Float64Array.from ({
            length : n * n
        }, transpose ? (_ : any, i : any) : any => {
                return matrix [i % n] [i / n | 0];
            } : (_ : any, i : any) : any => {
                return matrix [i / n | 0] [i % n];
            });
        let x : number = 0;
        for (const i of groupIndex) {
            const x0 : number = x;
            const subgroupIndex : any = range (0, n).filter ((j : any) : any [] => {
                    return matrix [i * n + j] || matrix [j * n + i];
                });
            for (const j of subgroupIndex) {
                let chord ! : any;
                if (i < j) {
                    chord = chords [i * n + j] || chords [i * n + j];
                    this.source = {
                        index : i,
                        startAngle : x,
                        endAngle : x += matrix [i * n + j],
                        value : matrix [i * n + j]
                    };
                } else {
                    chord = chords [j * n + i] || chords [j * n + i];
                    this.target = {
                        index : i,
                        startAngle : x,
                        endAngle : x += matrix [i * n + j],
                        value : matrix [i * n + j]
                    };
                }
            }
        }
        chords.groups = groups;
    }
    return chord;
}
