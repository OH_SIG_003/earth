type JSDynamicObject = any;
var obj : JSDynamicObject = {
    a : 1,
    b : 2
};
console.log (obj);
obj = {
    a : 3,
    b : 4
};
console.log (obj);
var eobj ! : JSDynamicObject;
if (obj.a == 3) {
    eobj = {
        a : 5,
        b : 6
    };
} else {
    eobj = {
        a : 7,
        b : 8
    };
}
console.log (eobj);
class Main {
    cobj : JSDynamicObject = {
        b : 7,
        c : 8
    };
    constructor () {
        console.log (this.cobj);
    }
}
const mainobj : Main = new Main ();
var fobj ! : JSDynamicObject;
function f () : JSDynamicObject {
    fobj = {
        a : 9,
        b : 10
    };
    obj = {
        a : 11,
        b : 12
    };
    return fobj;
}
console.log (f ());
console.log (obj);
