type JSDynamicObject = any;
const collection : JSDynamicObject = {
    one : 1,
    two : 2,
    three : 3,
    [Symbol.iterator] () : Iterator <symbol> {
        const values : string [] = Object.keys (this);
        let i : number = 0;
        return {
            next : () : JSDynamicObject => {
                    return {
                        value : this [values [i ++]],
                        done : i > values.length
                    };
                }
        };
    }
};
const iterator : any = collection [Symbol.iterator] ();
console.log (iterator.next ());
console.log (iterator.next ());
console.log (iterator.next ());
console.log (iterator.next ());
for (const value of collection) {
    console.log (value);
}
