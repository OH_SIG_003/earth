class Employee {
    static static_headcount : number;
    static static_personcount () : number {
        return Employee.static_headcount;
    }
    dynamic_headcount ! : number;
    dynamic_personcount () : number {
        return this.dynamic_headcount;
    }
    constructor (firstName : string, lastName : string, jobTitle : string) {
        Employee.static_headcount ++;
    }
}
Employee.static_headcount = 1;
console.log (Employee.static_headcount);
let personcount1 : number = Employee.static_personcount ();
console.log (personcount1);
let john : Employee = new Employee ('John', 'Doe', 'Front-end Developer');
john.dynamic_headcount = 2;
console.log (john.dynamic_headcount);
let personcount2 : number = john.dynamic_personcount ();
console.log (personcount2);
