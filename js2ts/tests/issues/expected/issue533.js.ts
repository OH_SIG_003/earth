var a : any [] = [0, 1, 2];
console.log (typeof a);
console.log (a);
console.log (a.length);
a [4294967295] = "not an array element";
console.log (typeof a);
console.log (a);
console.log (a.length);
console.log (a [4294967295]);
assert_sameValue (a.length, 3, 'a.length');
function assert_sameValue (l1 : number, l2 : number, message : string) : void {
    if (l2 !== l1) console.log (l2, " not equal ", message);
    else console.log (l2, " equal ", message);
}
