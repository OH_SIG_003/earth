function f1 (x : string | number) : void {
    x = (x as any) + 42;
    console.log (x);
}
f1 (0);
f1 ("abc ");
function f2 (x : string | number, y : number | string) : number | string {
    if (Number (x) == 1) {
        return (x as any) + 41;
    } else {
        return y;
    }
}
console.log (f2 (1, "def"));
console.log (f2 ("abc", 42));
function f3 (x : number | string) : void {
    if (String (x) == "abc") {
        console.log (x);
    }
    console.log (99);
}
var x : number | string = 42;
x = "abc";
f3 (x);
class C {
    f1 (x : number | string) : void {
        x = (x as any) + 42;
        console.log (x);
    }
    static f2 (x : number | string, y : string | number) : string | number {
        if (Number (x) == 1) {
            return (x as any) + 41;
        } else {
            return y;
        }
    }
    f3 (x : number | string) : void {
        if (String (x) == "abc") {
            console.log (x);
        }
        console.log (99);
    }
}
var o : C = new C ();
o.f1 (0);
o.f1 ("abc ");
console.log (C.f2 (1, "def"));
console.log (C.f2 ("abc", 42));
var z : boolean | number | string = true;
z = 99;
z = "abc";
o.f3 (x);
