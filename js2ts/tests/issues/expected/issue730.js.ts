const bigInt : bigint = 9007199254740991n;
const bigIntConstructorRep : bigint = BigInt (9007199254740991);
const bigIntStringRep : bigint = BigInt ("9007199254740991");
const previousMaxNum : bigint = BigInt (Number.MAX_SAFE_INTEGER);
