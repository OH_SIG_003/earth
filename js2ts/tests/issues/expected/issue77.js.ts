async function i () : Promise <number> {
    return 1;
}
async function j () : Promise <string> {
    return "abc";
}
async function k (x : number, y : number, z : number) : Promise <number> {
    return x + y + z;
}
console.log (k (1, 2, 3));
