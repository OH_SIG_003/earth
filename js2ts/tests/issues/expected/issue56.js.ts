type JSDynamicObject = any;
var obj : JSDynamicObject = {
    get [Symbol.iterator] () : any {
        console.log ('it should not get Symbol.iterator');
        return 99;
    },
    [Symbol.asyncIterator] () : Promise <IteratorResult<any, symbol>> {
        return {
            next () : number {
                return 42;
            }
        };
    }
};
class C {
    callCount : number = 0;
    async * #gen () : any {
        this.callCount += 1;
        yield * obj;
        console.log ('abrupt completion closes iter');
    }
    get gen () : any {
        return this.#gen;
    }
}
const c : C = new C ();
console.log (c.gen ());
