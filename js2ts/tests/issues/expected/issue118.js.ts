type JSDynamicObject = any;
var p : JSDynamicObject = {};
p.name = "tom";
p.age = 18;
var o1 : JSDynamicObject = {};
o1.value1 = 1;
o1.value2 = 2;
o1.value3 = 3;
o1.value4 = 4;
o1.value5 = 5;
var o2 : JSDynamicObject = {};
o2.value1 = "a";
o2.value2 = "b";
o2.value3 = "c";
o2.value4 = "d";
o2.value5 = "e";
var o3 : JSDynamicObject = {
    x : 1
};
o3.x = 2;
