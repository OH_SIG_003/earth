type JSDynamicObject = any;
function isNegative (number_ : number) : boolean {
    return number_ < 0;
};
function absolute (number_ : any) : number {
    return Math.abs (number_);
};
function getNumberUnitFormat (number_ : number, unit : string) : JSDynamicObject {
    if (! number_) {
        return {
            negative : false,
            format : ''
        };
    }
    if (isNegative (number_)) {
        return {
            negative : true,
            format : `${absolute(number_)}${unit}`
        };
    }
    return {
        negative : false,
        format : `${number_}${unit}`
    };
}
console.log (getNumberUnitFormat (- 10, 'cm'));
