type JSDynamicObject = any;
function _defineProperty (obj : JSDynamicObject, key : string, value ? : number) : JSDynamicObject {
    if (key in obj) {
        Object.defineProperty (obj, key, {
            value : value,
            enumerable : true,
            configurable : true,
            writable : true
        });
    } else {
        obj [key] = value;
    }
    return obj;
}
let person : JSDynamicObject = {};
_defineProperty (person, 'name', void 0);
_defineProperty (person, 'age', 21);
console.log (person);
