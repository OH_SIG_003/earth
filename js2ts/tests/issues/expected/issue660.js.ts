type JSDynamicObject = any;
var o : any = require ("ospec");
o.spec ("Simple Test", function () : void {
    var i : number = 0;
    var root : JSDynamicObject = {
        appendChild : {
            callCount : 0
        },
        insertBefore : {
            callCount : 0
        },
        childNodes : []
    };
    var test : JSDynamicObject = {
        expected : {
            creations : 0,
            moves : 0
        }
    };
    void [{
        delMax : 0,
        movMax : 50,
        insMax : 9
    }, {
        delMax : 3,
        movMax : 5,
        insMax : 5
    },].forEach (function (c : any) : void {
        var tests : number = 2;
        while (tests) {
            console.log (tests);
            o (i ++ + ": Test " + tests, function () : void {
                console.log ("Running test " + tests);
                if (root.appendChild.callCount + root.insertBefore.callCount !== test.expected.creations + test.expected.moves) console.log (test, {
                    aC : root.appendChild.callCount,
                    iB : root.insertBefore.callCount
                }, [].map.call (root.childNodes, function (n : any) : any {
                    return n.nodeName.toLowerCase ();
                }));
                o (root.appendChild.callCount + root.insertBefore.callCount).equals (test.expected.creations + test.expected.moves) ("moves");
            })
            tests -= 1;
        }
    })
})
