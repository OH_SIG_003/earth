// Create a FinalizationRegistry instance
const registry = new FinalizationRegistry(key => {
  console.log(`Finalizing object with key ${key}`);
});

// Create an object to be tracked by the registry
var obj = { name: "John" };

// Register the object with the registry
var ret = registry.register(obj, "myKey");

// Check if the object is still reachable
console.log(ret); // Output: true

// Unregister the object from the registry
ret = registry.unregister(obj);

console.log(ret); // Output: false

// Create a weak reference to the object
const weakRef = new WeakRef(obj);

// Register the weak reference with the registry
ret = registry.register(weakRef, "myKey");

// Check if the object is still reachable
console.log(ret); // Output: true

// Dereference the object
obj = null;

// Call the cleanup callback for the weak reference
var dref = weakRef.deref();

// Check if the object is still reachable
console.log(dref); // Output: false
