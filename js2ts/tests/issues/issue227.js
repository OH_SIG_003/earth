// union function results
g = false;
function f0 () {
    if (g == true) {
        return (1);
    } else {
        return ("abc");
    }
}

console.log (f0());

function f1 (x) {
    let y = 42;
    if (x == 1) {
        y = "abc";
    }
    return y;
}

console.log(f1(1));
console.log(f1(2));

function f2 (x,y) {
    if (x == 1) {
        return x + 41;
    } else {
        return y;
    }
}

console.log(f2(1,"def"));
console.log(f2(2,"abc"));

class C {
    f0 () {
        if (g == true) {
            return (1);
        } else {
            return ("abc");
        }
    }

    f1 (x) {
        let y = 42;
        if (x == 1) {
            y = "abc";
        }
        return y;
    }

    static f2 (x,y) {
        if (x == 1) {
            return x + 41;
        } else {
            return y;
        }
    }
}

o = new C();
console.log(o.f0());

console.log(o.f1(1));
console.log(o.f1(2));

console.log(C.f2(1,"def"));
console.log(C.f2(2,"abc"));

