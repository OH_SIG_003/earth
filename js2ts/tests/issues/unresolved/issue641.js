(function() {
    "use strict"
    function Vnode(tag, children) {
        return {tag: tag, children: children}
    }
    Vnode.normalize = function(node) {
        if (Array.isArray(node)) return Vnode("[", Vnode.normalizeChildren(node))
        if (node == null || typeof node === "boolean") return null
        if (typeof node === "object") return node
        return Vnode("#", String(node))
    }
    Vnode.normalizeChildren = function(input) {
        var children = []
        if (input.length) {
            for (var i = 0; i < input.length; i++) {
                children[i] = Vnode.normalize(input[i])
            }
        }
        return children
    }
    var input = [1, 2, 3];
    var normalizedChildren = Vnode.normalizeChildren(input);
    console.log(normalizedChildren);
}) ()
