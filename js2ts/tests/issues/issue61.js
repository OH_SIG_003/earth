// Type 'bigint' is not assignable to type 'number'
let left6 = 2n;
let right6 = 1n;
let result6 = left6 + right6;
console.log(result6);
