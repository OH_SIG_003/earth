var o = require("ospec")

o.spec("Simple Test", function() {
  var i = 0
  var root = {
    appendChild: { callCount: 0 },
    insertBefore: { callCount: 0 },
    childNodes: []
  }
  var test = {
    expected: { creations: 0, moves: 0 }
  }

  void [
    {delMax: 0, movMax: 50, insMax: 9},
    {delMax: 3, movMax: 5, insMax: 5},
  ].forEach(function(c) {
    var tests = 2
    while (tests) {
      console.log(tests)
      o(i++ + ": Test " + tests, function() {
        console.log("Running test " + tests)
        if (root.appendChild.callCount + root.insertBefore.callCount !== test.expected.creations + test.expected.moves) console.log(test, {aC: root.appendChild.callCount, iB: root.insertBefore.callCount}, [].map.call(root.childNodes, function(n){return n.nodeName.toLowerCase()}))

        o(root.appendChild.callCount + root.insertBefore.callCount).equals(test.expected.creations + test.expected.moves)("moves")
      })
      
      tests -= 1
    }
  })
})
