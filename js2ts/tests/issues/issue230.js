class C {
    p1 = 42;
    p2 = "abc";
    p3 = true;

    constructor () {
	this.p0 = 42;
    }
    
    f1 () {
	return 42;
    }
    f2 (x) {
	return x;
    }
    f3 () {
	return this.p1;
    }
    f4 (x) {
	return x;
    }
}

o1 = new C();
v0 = o1.p0;
console.log (v0);
v1 = o1.p1;
console.log (v1);
v2 = o1.p2;
console.log (v2);
if (o1.p3) {
    console.log (o1.p3);
}
v3 = o1.f1 ();
console.log (v3);
v4 = o1.f2 (v1);
console.log (v4);
v5 = o1.f3 ();
console.log (v5);
v6 = o1.f4 (v2);
console.log (v6);
