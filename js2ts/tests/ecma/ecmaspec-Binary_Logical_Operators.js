const a = 3;
const b = -2;

console.log(a > 0 && b > 0);
// Expected output: false
console.log(a > 0 || b > 0);
// Expected output: true

const foo = null ?? 'default string';
console.log(foo);
// Expected output: "default string"

const baz = 0 ?? 42;
console.log(baz);
// Expected output: 0