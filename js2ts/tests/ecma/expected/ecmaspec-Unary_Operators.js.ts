type JSDynamicObject = any;
let myVal : number = 10;
myVal ++;
console.log (myVal);
myVal --;
console.log (myVal);
let myVal1 : string = '10';
console.log ((+ myVal1));
console.log (- myVal1);
console.log (! myVal);
console.log (~ myVal);
let myFun : any = void function () : void {
    console.log ('This is my function');
}
console.log (myFun);
let myObj : JSDynamicObject = {
    name : 'abc',
    age : 20,
};
delete myObj.name;
console.log (myObj);
let myName : string = 'unknown';
console.log (typeof myName);
