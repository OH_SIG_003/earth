type JSDynamicObject = any;
let x : number = 2;
const y : number = 3;
console.log (x);
console.log (x = y + 1);
console.log (x = x * y);
let a : number = 1;
let b : number = 0;
a &&= 2;
console.log (a);
b &&= 2;
console.log (b);
const a1 : JSDynamicObject = {
    duration : 50,
    title : ''
};
a1.duration ||= 10;
console.log (a1.duration);
a1.title ||= 'title is empty.';
console.log (a1.title);
