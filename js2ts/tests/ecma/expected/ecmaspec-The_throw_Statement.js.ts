function getRectArea (width : number, height : number) : void {
    if (isNaN (width) || isNaN (height)) {
        throw new Error ('Parameter is not a number!');
    }
}
try {
    getRectArea (3, 4);
}
catch (e : any) {
    console.error (e);
}
