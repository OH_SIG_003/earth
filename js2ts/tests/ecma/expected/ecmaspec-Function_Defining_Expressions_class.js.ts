type JSDynamicObject = any;
class Apple {
    type : any;
    color : string;
    getInfo : () => string;
    constructor (type : any) {
        this.type = type;
        this.color = "red";
        this.getInfo = getAppleInfo;
    }
}
function getAppleInfo (this : JSDynamicObject) : string {
    return this.color + ' ' + this.type + ' apple';
}
