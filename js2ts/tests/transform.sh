#!/bin/bash
if [ $# -ne 1 ]; then
    echo "usage: ./transform.sh directory-path"
    echo ""
    echo "example: ./transform.sh C:/Users/Desktop/example"
    echo ""
    echo "This script converts all JavaScript files to TypeScript files and deletes the original JavaScript files. A backup of the directory will be created, retaining the same path and name, with the addition of a '-backup' suffix to the name."
    echo ""
    echo "Please ensure that the 'js2ts' file is located in the command path."
    exit 1
fi

CURRENT_DIR=$(pwd)

if [[ -d "$1" ]]; then
    :
else
    echo "The directory '$1' does not exist."
    exit 2
fi

echo "backup the directory"
PARENT_DIR=$(dirname "$1")
DEST_DIR="$PARENT_DIR/$(basename "$1")-backup"
cp -R "$1" "$DEST_DIR"

cd $1;

if [ -f "jsconfig.json" ]; then
jsconfig=$(<jsconfig.json)

tsconfig=$(echo "$jsconfig" | sed 's/"target":.*/"target": "es2021",/')
tsconfig=$(echo "$tsconfig" | sed 's/"module":.*/"module": "ESNext",/')
tsconfig=$(echo "$tsconfig" | sed 's/"strict":.*/"strict": true,/')
tsconfig=$(echo "$tsconfig" | sed 's/"esModuleInterop":.*/"esModuleInterop": true,/')
tsconfig=$(echo "$tsconfig" | sed 's/\.js"/.ts"/g')

echo "$tsconfig" > tsconfig.json

else
  file_names=()
  while IFS= read -r -d '' file; do
  file_names+=("${file%.js}.ts")
  done < <(find . -name "*.js" -print0)

  json_string='{
  "compilerOptions": {
    "target": "es2021",
    "module": "ESNext",
    "strict": true,
    "esModuleInterop": true
  },
  "files": [
'
  for file_name in "${file_names[@]}"; do
    json_string+="    \"$file_name\",\n"
  done
  json_string="${json_string%,\\n}
  ]
}"
echo -e "$json_string" > tsconfig.json

fi

echo "start transforming and generating js2ts.log"

echo "Generated TypeScript files:" > "$CURRENT_DIR/js2ts.log"

find . -name "*.js" -print0 | while IFS= read -r -d '' file; do

  echo "start processing the file $file"

  js2ts "$file" > "${file%.js}.ts"

  echo "The processing for the file $file has been successfully completed."

  echo "${file%.js}.ts" >> "$CURRENT_DIR/js2ts.log"
  echo "" >> "$CURRENT_DIR/js2ts.log"
  any_type_locations=$(grep -nE '(:|=>|=|\|)\s\bany\b' "${file%.js}.ts")
  if [ ! -z "$any_type_locations" ]; then
    echo "  Locations of the 'any' type:" >> "$CURRENT_DIR/js2ts.log"
    echo "$any_type_locations" | sed 's/^/    /' >> "$CURRENT_DIR/js2ts.log"
    echo "" >> "$CURRENT_DIR/js2ts.log"
  fi

union_type_locations=$(grep -nE ':\s[[:alpha:]]+(\s\[[^]]*\])?\s\|\s[[:alpha:]]+(\s\[[^]]*\])?\s(\|\s[[:alpha:]]+(\s\[[^]]*\])?\s)*' "${file%.js}.ts")
if [ ! -z "$union_type_locations" ]; then
  echo "  Locations of the 'union' type:" >> "$CURRENT_DIR/js2ts.log"
  echo "$union_type_locations" | sed 's/^/    /' >> "$CURRENT_DIR/js2ts.log"
  echo "" >> "$CURRENT_DIR/js2ts.log"
  
fi

  JSDynamicObject_locations=$(grep -nE '\bJSDynamicObject\b' "${file%.js}.ts")
  if [ ! -z "$JSDynamicObject_locations" ]; then
    echo "  Locations of the 'JSDynamicObject' type:" >> "$CURRENT_DIR/js2ts.log"
    echo "$JSDynamicObject_locations" | sed 's/^/    /' >> "$CURRENT_DIR/js2ts.log"
    echo "" >> "$CURRENT_DIR/js2ts.log"
  fi
  
  JSarguments_locations=$(grep -nE '\bJSarguments\b' "${file%.js}.ts")
  if [ ! -z "$JSarguments_locations" ]; then
    echo "  Locations of the 'JSarguments' type:" >> "$CURRENT_DIR/js2ts.log"
    echo "$JSarguments_locations" | sed 's/^/    /' >> "$CURRENT_DIR/js2ts.log"
    echo "" >> "$CURRENT_DIR/js2ts.log"
  fi

  JSNoValue_locations=$(grep -nE '\bJSNoValue\b' "${file%.js}.ts")
  if [ ! -z "$JSNoValue_locations" ]; then
  echo "  Locations of the 'JSNoValue' type:" >> "$CURRENT_DIR/js2ts.log"
  echo "$JSNoValue_locations" | sed 's/^/    /' >> "$CURRENT_DIR/js2ts.log"
  echo "" >> "$CURRENT_DIR/js2ts.log"
fi

  rm "$file"
done

FILENAME="js2ts.log"

# Check if the file exists in the current directory
if [[ -f "$CURRENT_DIR/$FILENAME" ]]; then
    echo "The transformation process has completed, and the "js2ts.log" file has been generated in the current directory $CURRENT_DIR."
else
    echo "The transformation process encountered an error. Please verify the path of the target directory and ensure that 'js2ts' is located in the command path."
fi
