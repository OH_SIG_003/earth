% JS2TS - convert JavaScript to TypeScript using type inference
% James Cordy, Huawei Technologies
% May 2022 (Rev. Aug 2023)

% Property member type closure rules - similar to closure-types.rul, but for class property members

% If we know the types of references to a class property member, we can infer its declaration type, and vice-versa

function propertyMemberClosureTypes
    replace [program]
        P [program]
    by
        P [_propertyMemberClosureTypes]         % references to property members inside the class itself
          [_dynamicPropertyMemberClosureTypes]  % references to property members of objects of the class
end function

% Private

rule _propertyMemberClosureTypes
    % Internal property member declarations and references, inside the class
    % For each class declaration
    replace $ [ClassDeclaration]
        Modifiers [AccessibilityModifier*] 'class ClassId [Identifier] TypeParameters [TypeParameters?]
            ClassHeritage [ClassHeritage]
        '{ ClassBody [ClassElement*] '} End [EOB]
    by
        Modifiers 'class ClassId TypeParameters ClassHeritage
        '{ ClassBody [_propertyMemberDeclarationClosureTypes ClassId ClassBody]
                     [_propertyMemberReferenceClosureTypes ClassId ClassBody]
        '} End
end rule

rule _propertyMemberDeclarationClosureTypes ClassId [Identifier] ClassBody [ClassElement*]
    % Infer each property member declaration type from the references to it of known type
    replace $ [ClassElement]
        Modifiers [AccessibilityModifier*] PropertyId [Identifier] Nullable [Nullable ?] ': DeclarationType [Type]
            Initializer [Initializer?] End [EOS]

    % Property references can be either static (ClassId.PropertyId) or instance (this.PropertyId)
    construct ThisId [Identifier]
        'this

    % Collect all references to the property of a known new type
    % The export-where-import paradigm allows us to explore the Scope and collect the references in it at the same time
    export NewReferenceTypes [KnownType*]
        _ % empty
    where
        ClassBody [_getNewPropertyReferenceTypes ClassId PropertyId DeclarationType]    % static property references
                  [_getNewPropertyReferenceTypes ThisId  PropertyId DeclarationType]    % instance property references
    import NewReferenceTypes

    % Do we have a new known type?
    construct NewDeclarationType [Type]
        DeclarationType [mergeType each NewReferenceTypes]
    deconstruct not NewDeclarationType
        DeclarationType

    % If so, then use it as the declaration type
    by
        Modifiers PropertyId Nullable ': NewDeclarationType Initializer End
end rule

% This function uses the TXL export-where-import paradigm to allow us explore the scope
% while simulataneously collecting items from it, in this case the types of property references

rule _getNewPropertyReferenceTypes ClassOrThisId [Identifier] PropertyId [Identifier] DeclarationType [Type]
    % Find each reference to the property of new known type in the class
    match $ [PrimaryExpression]
        ClassOrThisId '. PropertyId '(: NewReferenceType [KnownType] )
    deconstruct not * [KnownType] DeclarationType
        NewReferenceType
    % Add it to the set of new known types for the property
    import NewReferenceTypes [KnownType*]
    deconstruct not * [KnownType] NewReferenceTypes
        NewReferenceType
    export NewReferenceTypes
        NewReferenceTypes [. NewReferenceType]
end rule

rule _propertyMemberReferenceClosureTypes ClassId [Identifier] ClassBody [ClassElement*]
    % Infer the type of each property reference inside the class from its declaration of known type
    skipping [LeftHandSideExpression]
    replace $ [PrimaryExpression]
        ReferenceId [Identifier] '. PropertyId [Identifier] '(: ReferenceType [Type] )

    % Property references can be either static (ClassId.PropertyId) or instance (this.PropertyId)
    construct ClassNames [Identifier*]
        ClassId 'this
    % Is this a reference to a static or instance property of the class?
    deconstruct * [Identifier] ClassNames
        ReferenceId

    % Is it declared of a new known type in the class?
    skipping [ClassElement]
    deconstruct * [ClassElement] ClassBody
        Modifiers [AccessibilityModifier*] PropertyId Nullable [Nullable ?] ': DeclarationType [Type]
            Initializer [Initializer?] End [EOS]
    deconstruct not ReferenceType
        DeclarationType

    % Then change the reference's type
    by
        ReferenceId '. PropertyId '(: DeclarationType )
end rule

rule _dynamicPropertyMemberClosureTypes
    % Find every class declaration, and its following scope
    % (Class declarations are declaration-before-use in TS, so if it's used before that's a different problem)
    replace $ [SourceElement*]
        Modifiers [AccessibilityModifier*] 'class ClassId [Identifier] TypeParameters [TypeParameters?]
            ClassHeritage [ClassHeritage]
        '{ ClassBody [ClassElement*] '} End [EOB]
        Scope [SourceElement*]
    % Infer the type of property members of the class from the types of their object expression references
    by
        Modifiers 'class ClassId TypeParameters ClassHeritage
        '{ ClassBody [_dynamicPropertyMemberDeclarationClosureTypes ClassId ClassBody Scope]
        '} End
        Scope
end rule

rule _dynamicPropertyMemberDeclarationClosureTypes ClassId [Identifier] ClassBody [ClassElement*] Scope [SourceElement*]
    % Infer the type of a property member of the class from the types of its object expression references
    replace $ [ClassElement]
        Modifiers [AccessibilityModifier*] PropertyId [Identifier] Nullable [Nullable ?] ': DeclarationType [Type]  Initializer [Initializer?] End [EOS]

    % Collect all references to the property of a known new type
    % The export-where-import paradigm allows us to explore the Scope and collect the references in it at the same time
    export NewReferenceTypes [KnownType*]
        _ % empty
    where
        Scope [_getNewPropertyReferenceTypes ClassId PropertyId DeclarationType]                        % static property references
              [_getNewPrototypePropertyReferenceTypes ClassId PropertyId DeclarationType ]              % prototype property references
              [_getNewDynamicPropertyReferenceTypes ClassId PropertyId DeclarationType ClassBody Scope] % dynamic property references
    import NewReferenceTypes

    % Do we have a new known type?
    construct NewDeclarationType [Type]
        DeclarationType [mergeType each NewReferenceTypes]
    deconstruct not NewDeclarationType
        DeclarationType

    % If so, then update the declaration type
    by
        Modifiers PropertyId Nullable ': NewDeclarationType Initializer End
end rule

% This function uses the TXL export-where-import paradigm to allow us explore the scope
% while simulataneously collecting items from it, in this case the types of property prototype references

rule _getNewPrototypePropertyReferenceTypes ClassId [Identifier] PropertyId [Identifier] DeclarationType [Type]
    % Find each prototype reference to the property of new known type in the scope
    match $ [PrimaryExpression]
        ClassId '. 'prototype '. PropertyId '(: NewReferenceType [KnownType] )
    deconstruct not * [KnownType] DeclarationType
        NewReferenceType
    % Add it to the set of new known types for the prototype property
    import NewReferenceTypes [KnownType*]
    deconstruct not * [KnownType] NewReferenceTypes
        NewReferenceType
    export NewReferenceTypes
        NewReferenceTypes [. NewReferenceType]
end rule

% This function uses the TXL export-where-import paradigm to allow us explore the scope
% while simulataneously collecting items from it, in this case the types of dynamic object property references

rule _getNewDynamicPropertyReferenceTypes ClassId [Identifier] PropertyId [Identifier] DeclarationType [Type]
        ClassBody [ClassElement*] Scope [SourceElement*]
    % Find each reference to the property of new known type through an object in the scope
    match $ [PrimaryExpression]
        ObjectId [Identifier] '. PropertyId '(: NewReferenceType [KnownType] )
    % Is the object of the class?
    deconstruct * [SimpleLexicalBinding] Scope
        ObjectId _ [Nullable?] ': ClassId _ [Initializer?]
    % Add it to the set of new known types for the prototype property
    deconstruct not * [KnownType] DeclarationType
        NewReferenceType
    import NewReferenceTypes [KnownType*]
    deconstruct not * [KnownType] NewReferenceTypes
        NewReferenceType
    export NewReferenceTypes
        NewReferenceTypes [. NewReferenceType]
end rule
