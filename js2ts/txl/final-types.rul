% JS2TS - convert JavaScript to TypeScript using type inference
% James Cordy, Huawei Technologies
% May 2022 (Rev. Aug 2023)

% Final type decision rules

% Sometimes we can't infer a good type for lack of evidence, and we end up with only null or undefined.
% In these cases, there is nothing to do but use 'any'.

function finalTypes
    % Decide on a final type for cases that can't infer a reasonable type
    replace [program]
        P [program]
    by
        P [_nullUndefinedTypes]
          [_nullUndefinedArrayTypes]
          [_weakNullBooleanTypes]
          [_complexUnionTypes]
          [_complexMixedTypes]
          [_undefinedFormalTypes]
          [_undefinedFunctionTypes]
          [_finalFormalTypes]
          [_finalConstructorFunctionFormalTypes]
          [_finalConstructorFunctionTypes]
          [_finalParenthesizedTypes]
          [_finalAnyTypes]
          [_redundantAsAny]
end function

rule _nullUndefinedTypes
    % If we have only null and/or undefined, we don't really know
    construct AnyTypes [Type*]
        'null
        'undefined
        'null '| 'undefined
        'undefined '| 'null
    % If a type we inferred
    replace $ [Type]
        Type [Type]
    % Is one of those above, then we prefer "any"
    deconstruct * [Type] AnyTypes
        Type
    by
        'any
end rule

rule _weakNullBooleanTypes
    % Boolean inferences are weak, because every type in JS is truthy/falsy
    replace $ [Type]
        Type [Type]
    % If it's both boolean and null, it's pretty weak
    deconstruct * [IntersectionType] Type
        'boolean
    deconstruct * [IntersectionType] Type
        'null
    % So we prefer "any"
    by
        'any
end rule

rule _undefinedFormalTypes
    % Formal parameter types which include "undefined" are better expressed as optional
    replace [FormalParameter,]
        Modifiers [AccessibilityModifier*] BindingIdentifier [BindingIdentifier] _ [Nullable?]
            ': BaseType [UnionType] '| 'undefined Initializer [Initializer?]
        ', MoreFormals [FormalParameter,]
    % Only trailing formals can be optional, so make sure that any following this one are also optional
    deconstruct not * [Nullable?] MoreFormals
        % not optional
    deconstruct not * [Nullable?] MoreFormals
        '!  % not optional
    by
        % OK, make it optional
        Modifiers BindingIdentifier '?  ': BaseType Initializer
        ', MoreFormals
end rule

rule _undefinedFunctionTypes
    % Functions whose return type includes "undefined" may not return a value
    replace [CallSignature]
        Async ['async ?] TypeParameters [TypeParameters?] '( ParameterList [ParameterList] ') ': BaseType [UnionType] '| 'undefined
    % Give it a special type to indicate that's what is going on
    by
        Async TypeParameters '( ParameterList ') ': BaseType '| 'JSNoValue
end rule

rule _nullUndefinedArrayTypes
    % If we have a null and/or undefined array, we didn't have enough evidence to get its type
    % and it will cause problems in TS
    construct AnyTypes [Type*]
        'null '[ ']
        'undefined '[ ']
    replace $ [Type]
        Type [Type]
    % Is it one of those above?
    deconstruct * [Type] AnyTypes
        Type
    % Then "any" is better
    by
        'any '[ ']
end rule

rule _complexUnionTypes
    % Too many alternatives in a union means that it's probably intentionally untyped in JS
    replace $ [UnionType]
        UnionType [UnionType]
    % We limit the number of alternatives to 4
    deconstruct * [OrIntersectionType] UnionType
        _ [OrIntersectionType]
    construct Alternatives [IntersectionType*]
        _ [^ UnionType]
    construct NAlternatives [number]
        _ [length Alternatives]
    where
        NAlternatives [> 3]     % (sic) one base type plus three "or"s
    % Untyped is "any" in TS
    by
        'any
end rule

rule _complexMixedTypes
    construct ScalarTypes [list IntersectionType]
        'number, 'string, 'bigint, 'boolean
    construct UncertainArrayTypes [list IntersectionType]
        'any '[ '], 'any '[ '] '[ ']
    % Mixing arrays and scalars isn't usually right
    replace $ [UnionType]
        UnionType [UnionType]
    where
        UnionType [_containsType each ScalarTypes]
    where
        UnionType [_containsType each UncertainArrayTypes]
    % Untyped is "any" in TS
    by
        'any
end rule

function _containsType Type [IntersectionType]
    match * [IntersectionType]
        Type
end function

rule _finalFormalTypes
    % Formal parameters used with "typeof" are intentionally multi-type in JS,
    % and TS only allows "any" type for "typeof"
    replace $ [FunctionDeclaration]
        Async ['async ?] 'function Star ['* ?] Id [id] TypeParameters [TypeParameters?]
            '( Formals [list FormalParameter] ') ': Type [Type]
        Body [OptionalFunctionBody]
    % Typeof formals must be "any"
    by
        Async 'function Star Id TypeParameters '( Formals [_finalFormalType Body] ') ': Type
        Body
end rule

rule _finalFormalType Body [OptionalFunctionBody]
    % Look at each formal parameter
    skipping [FormalParameter]
    replace $ [FormalParameter]
        Spread [Spread?] FormalId [id] ': FormalType [Type] Initializer [Initializer?]
    % Is it used with "typeof"?
    deconstruct * [UnaryExpression] Body
        'typeof FormalId _ [ExpressionType?]
    % Then it should be type "any"
    by
        Spread FormalId ': 'any Initializer
end rule

rule _finalConstructorFunctionFormalTypes
    % JS constructor/prototype functions act like both functions and dynamic classes
    % When they reference their dynamic instance object using "this", in TS it is undefined
    replace $ [FunctionDeclaration]
        Async ['async ?] 'function Star ['* ?] FunctionId [BindingIdentifier?] TypeParameters [TypeParameters?]
            '( Formals [list FormalParameter] ') ': Type [Type]
        Body [OptionalFunctionBody]
    % Does this function reference "this"?
    deconstruct * [PrimaryExpression] Body
        'this _ [DotFieldIdentifier*] '(: _ [Type] )
    % The it's a dynamic class, and "this" is its implicit dynamic instance object
    % We add it explciitly as a first formal parameter in TS
    by
        Async 'function Star FunctionId TypeParameters '( 'this ': 'JSDynamicObject ', Formals ') ': Type
        Body
end rule

rule _finalConstructorFunctionTypes
    % JS constructor/prototype functions act like both functions and dynamic classes
    % They must be of type "void"
    replace $ [SourceElement*]
        Async ['async ?] 'function Star ['* ?] FunctionId [Identifier] TypeParameters [TypeParameters?]
            '( Formals [list FormalParameter] ') ': Type [Type]
        Body [OptionalFunctionBody]
        Scope [SourceElement*]
    deconstruct not Type
        'void
    % Is this function a constructor function?
    deconstruct * [MemberExpression] Scope
        'new FunctionId '(: _ [Type] ) _ [MemberComponents]
    % Then it has to be void
    by
        Async 'function Star FunctionId TypeParameters '( Formals ') ': 'void
        Body
        Scope
end rule

rule _finalParenthesizedTypes
    % As part of the transformation we may have to parenthesize unions and other types
    % that are later simplified to a single type
    replace $ [PrimaryType]
        '( PrimaryType [PrimaryType] ')
    % They look better if they are unparenthesized in our output
    by
        PrimaryType
end rule

rule _finalAnyTypes
    % Since we use "any" as the default "untyped" type in our type inference process,
    % we need a special type for the case where the result is intentionally "any"
    replace $ [Type]
        Type [Type]
    % "JSany" is an internal type we use for this purpose
    deconstruct * [PrimaryType] Type
        'JSany
    % In final output, it's simply "any"
    by
        'any
end rule

rule _redundantAsAny
    % In an array type, any is a primary type
    construct PrimaryAny [PrimaryType]
        'any
    % So the annotation could be either
    construct Anys [Type*]
        'any PrimaryAny
    % Either way, the "as any" is redundant
    replace $ [UnaryExpression]
        SubPrimaryExpression [SubPrimaryExpression] '(: Type [Type] ) 'as 'any
    deconstruct * [Type] Anys
        Type
    by
        SubPrimaryExpression '(: 'any)
end rule
