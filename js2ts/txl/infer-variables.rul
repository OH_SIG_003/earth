% js2ts - convert javascript to typescript using type inference
% James Cordy, Huawei Technologies
% May 2022 (Rev. Aug 2023)

% Variable inference rules - infer the need for variable declarations
% for assigned but undeclared variables

function inferVariables
    replace [program]
        GlobalElements [SourceElement*]
    by
        GlobalElements [_inferGlobalVariables]
                       [_inferFunctionVariables]
                       [_inferPropertyMembers]
                       [_inferExternalStaticPropertyMembers]
                       [_inferExternalPrototypePropertyMembers]
                       [_joinInitializedVariables]
                       [_nonNullUninitializedVariables]
                       [_nonNullUninitializedPropertyMembers]
end function

% Private

% Infer the existence of implicitly declared variables in the global scope
% In JS, undeclared variables are implicitly global, not local to a function as one might expect

function _inferGlobalVariables
    % Look through the entire program for undeclared global variables
    replace [SourceElement*]
        GlobalScope [SourceElement*]
    % Collect all the already declared global variables in the program
    % The export-where-import paradigm allows us to explore the global Scope
    % and collect the declarations at the same time
    export GlobalDeclarations [Identifier*]
        'this 'super    % predeclared variables - we don't want to mistake these for undeclared globals
    where
        GlobalScope [_getGlobalDeclarations]
                    [_orNoDeclarations]         % possibly there are none, but we want to continue anyway
    by
        GlobalScope [_inferEachGlobalVariable]
end function

% This function uses the TXL export-where-import paradigm to allow us explore the scope
% while simulataneously collecting items from it, in this case globally declared names

rule _getGlobalDeclarations
    % Find a globally declared (bound) identifier
    skipping [FunctionOrClassDeclaration]
    match $ [BindingIdentifier]
        Identifier [Identifier]
    % Add it to the set
    import GlobalDeclarations [Identifier*]
    export GlobalDeclarations
        Identifier
        GlobalDeclarations
end rule

function _orNoDeclarations
    % Always succeeds
    match [SourceElement*]
        _ [SourceElement*]
end function

function _inferEachGlobalVariable
    % This recursive function walks through the top-level global elements of the program,
    % finding each (posibly deeply) assigning global element and inserting declarations for
    % any assigned variables not yet declared

    % It stops when there are no more assigning global elements

    % Find the next global element
    skipping [SourceElement]                        % e.g.:
    replace * [SourceElement*]
        NextElements [SourceElement*]               % x = 10;
                                                    % y = 11;
                                                    % x = y;
    deconstruct NextElements
        AssigningElement [SourceElement]            % x = 10;
        MoreElements [SourceElement*]               % y = 11;
                                                    % x = y;
    % Get all of the non-local assigned variables in it
    construct AssignedVariables [LeftHandSideExpression*]
        _ [^ AssigningElement]
          [_removeLocalVariables]
          [_removeDuplicateVariables]               % x

    % Does this element assign any variables?
    % If no, the search continues to the next "assigning element"
    deconstruct not AssignedVariables
        % empty

    % If yes, then create new global declarations for each of them that's not already declared
    construct NewDeclarations [SourceElement*]
        _ [_declareImplicitVariables each AssignedVariables]

    % Then recursively find next assigning global element
    construct NewMoreElements [SourceElement*]
        MoreElements [_inferEachGlobalVariable]     % var y !;
                                                    % y = 11;
                                                    % x = y;
    by
        NewDeclarations                             % var x !;
          [. AssigningElement]                      % x = 10;
          [. NewMoreElements]                       % var y !;
                                                    % y = 11;
                                                    % x = y;
end function

rule _removeDuplicateVariables
    % We need only one copy of each assigned variable
    replace [LeftHandSideExpression*]
        AssigningExpression [LeftHandSideExpression]
        More [LeftHandSideExpression*]
    % Is there another copy?
    deconstruct * [LeftHandSideExpression] More
        AssigningExpression
    % Then remove this copy
    by
        More
end rule

rule _removeLocalVariables
    % After unique renaming (see rename-variables.rul), we can distinguish
    % local variables from other variables by their unique name
    replace [LeftHandSideExpression*]
        AssigningExpression [LeftHandSideExpression]
        More [LeftHandSideExpression*]
    % Get the assigned identifier
    deconstruct * [Identifier] AssigningExpression
        Id [Identifier]
    % Is it a local variable name?
    where
        Id [_isLocalVariable]
    % Yes, then remove it
    by
        More
end rule

function _isLocalVariable
    % After unique renaming, we can distinguish local variables by their unique name,
    % which is of the form "x_!_N" for some number N
    match [Identifier]
        Id [id]
    where
        Id [grep "_!_"]
end function

function _declareImplicitVariables AssignedVariable [LeftHandSideExpression]
    % For each assigned left hand side, we see if the main variable is already declared
    deconstruct AssignedVariable
        UndeclaredVariableId [Identifier] Components [MemberComponent*]
    % Is it declared?
    import GlobalDeclarations [Identifier*]
    deconstruct not * [Identifier] GlobalDeclarations
        UndeclaredVariableId
    % If not, then make a declaration for it
    replace * [SourceElement*]
        % end
    % And remember that it's now declared
    export GlobalDeclarations
        UndeclaredVariableId
        GlobalDeclarations
    by
        'var UndeclaredVariableId '! ;
end function

% Improve style of inferred global variables

rule _joinInitializedVariables
    % The output looks nicer if an inferred variable declaration and an immediate assignment to it
    % are joined, for example "var x!; x = 42;" becomes "var x = 42;"
    replace [SourceElement*]
        'var Id [Identifier] _ [Nullable?] _ [EOS]
        Id = Expression [AssignmentExpression] End [EOS]
        More [SourceElement*]
    by
        'var Id = Expression End
        More
end rule

rule _inferFunctionVariables
    % In JS, function declarations can be assigned as variables, but in TS, only variables can be assigned
    % If one was assigned, we've already inferred a variable declaration for it -
    % now if it's also a function declaration, we need to convert that into an assignment to the variable

    % For each global variable declaration
    replace $ [SourceElement*]
        'var Id [Identifier] Nullable [Nullable?] End [EOS]
        More [SourceElement*]
    % If there's a function declaration of the same name, convert it into an assignment to the variable
    by
        'var Id Nullable End
        More [_assignFunctionVariableDeclaration Id]
end rule

function _assignFunctionVariableDeclaration VarId [Identifier]
    % Is there a function declaration of this name?
    skipping [SourceElements]
    replace * [SourceElement]
        Async ['async ?] 'function Star ['* ?] VarId CallSignature [CallSignature] OptionalFunctionBody [OptionalFunctionBody]
    % Then turn it into an assignment to the function variable
    by
        VarId = Async 'function Star CallSignature OptionalFunctionBody
end function

% Class member inference rules

% In each class body, we infer a declaration for property X from this.X references, when X is not already declared
% (Not yet done: infer structured class properties X.Y from this.X.Y references)

rule _inferPropertyMembers
    % For each class body in the program
    replace $ [ClassBody]
        ClassBody [ClassBody]

    % Get all the "this.x" assignments in the class body (including this.x.* assignments)
    construct ThisMembers [LeftHandSideExpression*]
        _ [^ ClassBody]         % extracts all variables assigned in the class
          [_thisMembersOnly]    % but we're only interested in ones of the form "this.x"

    % (Optional) reorder them into reverse order of reference, so that we can declare them in original order
    construct OrderedThisMembers [LeftHandSideExpression*]
          _ [_addThisMember each ThisMembers]

    % Add a member declaration for each of them (possibly none)
    by
        ClassBody [_addInferredPropertyMember each OrderedThisMembers]
                  [_addInferredPropertyIndex each OrderedThisMembers]
end rule

rule _thisMembersOnly
    % We're only interested in assigned "this.x" references
    skipping [LeftHandSideExpression]
    replace [LeftHandSideExpression*]
        LeftHandSideExpression [LeftHandSideExpression]
        Rest [LeftHandSideExpression*]
    % If it isn't a "this." reference, remove it
    deconstruct not LeftHandSideExpression
        'this FieldIdentifiers [DotFieldIdentifier*] More [MemberComponents]
    by
        Rest
end rule

function _addThisMember ThisMember [LeftHandSideExpression]
    % Used to reverse the set of this references by adding them at the beginning of the set
    replace [LeftHandSideExpression*]
        ThisMembersSoFar [LeftHandSideExpression*]
    by
        ThisMember
        ThisMembersSoFar
end function

function _addInferredPropertyMember ThisMember [LeftHandSideExpression]
    % For each assigned reference "this.x", add a member declaration for "x" if there isn't one already
    deconstruct ThisMember
        'this '. PropertyName [Identifier] More [DotFieldIdentifier*]
    % Is there a declaration for the property or function "x" in the class?
    replace [ClassBody]
        ClassElements [ClassElement*]
    deconstruct not * [ClassElement] ClassElements
        _ [AccessibilityModifier*] PropertyName _ [Nullable ?] _ [TypeAnnotation?] _ [Initializer?] _ [EOS]
    deconstruct not * [ClassElement] ClassElements
        _ [AccessibilityModifier*] _ ['async ?] _ ['* ?] _ [getOrSet?] PropertyName _ [Nullable ?] _ [CallSignature] _ [OptionalFunctionBody]
    % No, so add a declaration for it at the beginning of the class
    by
        PropertyName ';
        ClassElements
end function

function _addInferredPropertyIndex ThisMember [LeftHandSideExpression]
    % If there is an assigned index reference "this[x]", add an index member to the class if there isn't one already
    deconstruct ThisMember
        'this '[ _ [AssignmentExpression] '] More [MemberComponent*]
    % Does the class already have an index member?
    replace [ClassBody]
        ClassElements [ClassElement*]
    deconstruct not * [ClassElement] ClassElements
        _ [AccessibilityModifier*] '[ 'key ': 'string  '] ': _ [Type] _ [EOS]
    % No, so add one to the class
    by
        '[ 'key ': 'string '] ': 'any  ';
        ClassElements
end function

rule _nonNullUninitializedVariables
    % Variables must be initialized before use in TS, but sometimes TS cannot tell if they are
    % To avoid that error message, we mark them as non-null using !
    replace [LexicalDeclaration]
        LetOrConst [LetOrConst] BindingIdentifier [BindingIdentifier] TypeAnnotation [TypeAnnotation?] End [EOS]
    by
        LetOrConst BindingIdentifier '! TypeAnnotation End
end rule

rule _nonNullUninitializedPropertyMembers
    % Properties must be intiialized before use in TS, but sometimes TS cannot tell if they are
    % To avoid that error message, we mark them as non-null using !
    replace $ [ClassElement*]
        Modifiers [AccessibilityModifier*] PropertyName [Identifier] TypeAnnotation [TypeAnnotation?] End [EOS]
        MoreClassElements [ClassElement*]
    % In TS, static properties are implicitly initialized to "undefined"
    deconstruct not * [AccessibilityModifier] Modifiers
        'static
    % Is it initialized in the constructor?
    construct NonNull [Nullable?]
        _ [_nonNullIfNotAssignedInConstructor MoreClassElements PropertyName]
    by
        Modifiers PropertyName NonNull TypeAnnotation End
        MoreClassElements
end rule

function _nonNullIfNotAssignedInConstructor ClassElements [ClassElement*] PropertyName [Identifier]
    % Does the class have a constructor?
    skipping [ClassDeclaration]
    deconstruct * [ConstructorDeclaration] ClassElements
        _ [AccessibilityModifier*] 'constructor _ [Parameters] ConstructorBody [OptionalFunctionBody]
    % Does it always assign to the property?
    skipping [Statement]
    deconstruct not * [Statement] ConstructorBody
        'this '. PropertyName '= _ [AssignmentExpression] _ [EOS]
    % No, so we need to assert it's non-null
    replace [Nullable?]
    by
        '!
end function

rule _inferExternalStaticPropertyMembers
    % Sometimes there are no internal "this.x" assignments to a class property,
    % but we can infer its existence from external assignments, and add a declaration for it

    % This is the static property case, see below for the prototype property case
   replace $ [SourceElement*]
        Modifiers [AccessibilityModifier*] 'class ClassId [Identifier] TypeParameters [TypeParameters?] ClassHeritage [ClassHeritage] '{
            ClassBody [ClassElement*]
        '} End [EOB]
        Scope [SourceElement*]          % Note: in JS, technically the scope of the class may include source above it
                                        %   but for now we ignore that rare case

    % Collect all externally assigned static property member variables
    % The export-where-import paradigm allows us to explore the global Scope
    % and collect the assigned static class members at the same time
    export AssignedStaticPropertyMembers [LeftHandSideExpression*]
        _ % empty
    where
        Scope [_getAssignedStaticPropertyMembers ClassId]
    import AssignedStaticPropertyMembers

    % For each of them we found, infer a static property declaration
    by
        Modifiers 'class ClassId TypeParameters ClassHeritage '{
            ClassBody [_inferAssignedStaticProperty each AssignedStaticPropertyMembers]
        '} End
        Scope
end rule

% This function uses the TXL export-where-import paradigm to allow us explore the scope
% while simulataneously collecting items from it, in this case assigned static class properties

rule _getAssignedStaticPropertyMembers ClassId [Identifier]
    % Find each assigned static property member of the class in the scope
    match $ [LeftHandSideExpression]
        ClassId '. PropertyId [Identifier]
    % Add it to the set if it's not already there
    import AssignedStaticPropertyMembers [LeftHandSideExpression*]
    deconstruct not * [LeftHandSideExpression] AssignedStaticPropertyMembers
        ClassId '. PropertyId
    export AssignedStaticPropertyMembers
        ClassId '. PropertyId
        AssignedStaticPropertyMembers
end rule

function _inferAssignedStaticProperty AssignedStaticProperty [LeftHandSideExpression]
    % Infer a property declaration for an assigned static property if it's not already declared
    deconstruct AssignedStaticProperty
        ClassId [Identifier] '. PropertyId [Identifier]
    deconstruct not PropertyId
        'prototype
    % Is it already declared?
    replace [ClassElement*]
        ClassElements [ClassElement*]
    skipping [ClassBody]
    deconstruct not * [MemberVariableDeclaration] ClassElements
        'static PropertyId _ [Nullable ?] _ [TypeAnnotation?] _ [Initializer?] _ [EOS]
    % If not, add a declaration for it at the beginning of the class
    by
        'static PropertyId ';
        ClassElements
end function

rule _inferExternalPrototypePropertyMembers
    % Sometimes there are no internal "this.x" assignments to a class property,
    % but we can infer its existence from external assignments, and add a declaration for it

    % This is the prototype property case, see above for the static property case
   replace $ [SourceElement*]
        Modifiers [AccessibilityModifier*] 'class ClassId [Identifier] TypeParameters [TypeParameters?] ClassHeritage [ClassHeritage] '{
            ClassBody [ClassElement*]
        '} End [EOB]
        Scope [SourceElement*]          % Note: in JS, technically the scope of the class may include source above it
                                        %   but for now we ignore that rare case

    % Collect all externally assigned prototype property member variables
    % The export-where-import paradigm allows us to explore the global Scope
    % and collect the assigned prototype members at the same time
    export AssignedPrototypePropertyMembers [LeftHandSideExpression*]
        _ % empty
    where
        Scope [_getAssignedPrototypePropertyMembers ClassId]
    import AssignedPrototypePropertyMembers

    % For each of them we found, infer a property declaration
    by
        Modifiers 'class ClassId TypeParameters ClassHeritage '{
            ClassBody [_inferAssignedPrototypeProperty each AssignedPrototypePropertyMembers]
        '} End
        Scope
end rule

% This function uses the TXL export-where-import paradigm to allow us explore the scope
% while simulataneously collecting items from it, in this case assigned prototype class properties

rule _getAssignedPrototypePropertyMembers ClassId [Identifier]
    % Find each assigned prototype property of the class in the scope
    match $ [LeftHandSideExpression]
        ClassId '. 'prototype '. PropertyId [Identifier]
    % Add it to the set if it's not already there
    import AssignedPrototypePropertyMembers [LeftHandSideExpression*]
    deconstruct not * [LeftHandSideExpression] AssignedPrototypePropertyMembers
        ClassId '. PropertyId
    export AssignedPrototypePropertyMembers
        ClassId '. PropertyId
        AssignedPrototypePropertyMembers
end rule

function _inferAssignedPrototypeProperty AssignedPrototypeProperty [LeftHandSideExpression]
    % Infer a property declaration for an assigned prototype property if it's not already declared
    deconstruct AssignedPrototypeProperty
        ClassId [Identifier] '. PropertyId [Identifier]
    % Is it already declared?
    replace [ClassElement*]
        ClassElements [ClassElement*]
    skipping [ClassBody]
    deconstruct not * [MemberVariableDeclaration] ClassElements
        _ [AccessibilityModifier*] PropertyId _ [Nullable ?] _ [TypeAnnotation?] _ [Initializer?] _ [EOS]
    skipping [ClassBody]
    deconstruct not * [MemberFunctionDeclaration] ClassElements
        _ [AccessibilityModifier*] _ ['async ?] PropertyId _ [Nullable ?] _ [CallSignature] _ [OptionalFunctionBody]
    % If not, add a declaration for it at the beginning of the class
    by
        PropertyId '! ';
        ClassElements
end function
