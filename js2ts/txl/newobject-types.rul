% JS2TS - convert JavaScript to TypeScript using type inference
% James Cordy, Huawei Technologies
% May 2022 (Rev. Aug 2023)

% New object type rules

% We can infer that the type of a new() call is the type of its class

function newObjectTypes
    replace [program]
        P [program]
    by
        P [_dynamicBuiltinNewObjectTypes]       % Untyped dynamic objects first
          [_dynamicNewObjectTypes]
          [_simpleNewObjectTypes P]             % Then simple named class objects
          [_simpleNewObjectTypesNoArguments P]
          [_memberNewObjectTypes P]             % And simple named subclass objects
          [_memberNewObjectTypesNoArguments P]
end function

% Private

rule _simpleNewObjectTypes P [program]
    % If we don't know the type of the new expression, and it's not already typed
    % as an untyped dynamic object (JSDynamicObject), then it must be an object of the named class
    replace $ [PrimaryExpression]
        % All new expressions have been parenthesized by parenthesize-expressions.rul to allow a type annotation
        '( 'new ClassId [Identifier] '(: 'any ) Arguments [Arguments] ') '(: 'any )
    % We can't use variables as class types
    deconstruct not * [SimpleLexicalBinding] P
        ClassId _ [Nullable?] _ [TypeAnnotation?] _ [Initializer?]
    deconstruct not ClassId
        'this
    % We can't use prototype functions as class types
    deconstruct not * [FunctionDeclaration] P
        _ ['async ?] 'function _ ['* ?] ClassId _ [CallSignature] _ [OptionalFunctionBody]
    % OK, it's a class typed new expression
    construct ClassType [Type]
        ClassId
    by
        '( 'new ClassId '(: ClassType ) Arguments ') '(: ClassType ')
end rule

rule _simpleNewObjectTypesNoArguments P [program]
    % Same as above, but without arguments
    replace $ [PrimaryExpression]
        % All new expressions have been parenthesized by parenthesize-expressions.rul to allow a type annotation
        '( 'new ClassId [Identifier] '(: 'any ) ') '(: 'any )
    % We can't use variables as class types
    deconstruct not * [SimpleLexicalBinding] P
        ClassId _ [Nullable?] _ [TypeAnnotation?] _ [Initializer?]
    deconstruct not ClassId
        'this
    % We can't use prototype functions as class types
    deconstruct not * [FunctionDeclaration] P
        _ ['async ?] 'function _ ['* ?] ClassId _ [CallSignature] _ [OptionalFunctionBody]
    % OK, it's a class typed new expression
    construct ClassType [Type]
        ClassId
    by
        '( 'new ClassId '(: ClassType ) ') '(: ClassType ')
end rule

rule _memberNewObjectTypes P [program]
    % Same as two above, but for member subclasses
    replace $ [PrimaryExpression]
        % All new expressions have been parenthesized by parenthesize-expressions.rul to allow a type annotation
        '( 'new ObjectId [Identifier] '. ClassId [FieldIdentifier] '(: 'any ) Arguments [Arguments] ') '(: 'any )
    % Without a class name, we have no class type
    deconstruct not ClassId
        'constructor
    deconstruct not ClassId
        'this
    % We can't use variables as class types
    deconstruct not * [SimpleLexicalBinding] P
        ObjectId _ [Nullable?] _ [TypeAnnotation?] _ [Initializer?]
    % We can't use prototype functions as class types
    deconstruct not * [FunctionDeclaration] P
        _ ['async ?] 'function _ ['* ?] ObjectId _ [CallSignature] _ [OptionalFunctionBody]
    % OK, it's a class typed new expression
    construct ClassType [Type]
        ObjectId '. ClassId
    by
        '( 'new ObjectId '. ClassId '(: ClassType ) Arguments ') '(: ClassType )
end rule

rule _memberNewObjectTypesNoArguments P [program]
    % Same as above, but without arguments
    replace $ [PrimaryExpression]
        % All new expressions have been parenthesized by parenthesize-expressions.rul to allow a type annotation
        '( 'new ObjectId [Identifier] '. ClassId [FieldIdentifier] '(: 'any ) ') '(: 'any )
    % Without a class name, we have no class type
    deconstruct not ClassId
        'constructor
    deconstruct not ClassId
        'this
    % We can't use variables as class types
    deconstruct not * [SimpleLexicalBinding] P
        ObjectId _ [Nullable?] _ [TypeAnnotation?] _ [Initializer?]
    % We can't use prototype functions as class types
    deconstruct not * [FunctionDeclaration] P
        _ ['async ?] 'function _ ['* ?] ObjectId _ [CallSignature] _ [OptionalFunctionBody]
    % OK, it's a class typed new expression
    construct ClassType [Type]
        ObjectId '. ClassId
    by
        '( 'new ObjectId '. ClassId '(: ClassType ) ') '(: ClassType )
end rule

rule _dynamicBuiltinNewObjectTypes
    % Built-in classes Object and Proxy explicitly create new untyped dynamic objects
    construct DynamicBuiltinClasses [id*]
        'Object 'Proxy
    % Find every new expression
    replace $ [PrimaryExpression]
        '( 'new ClassId [id] '(: 'any ) Arguments [Arguments] ') '(: 'any )
    % Whose class is Object or Proxy
    deconstruct * [id] DynamicBuiltinClasses
        ClassId
    % And explicitly type it as untyped dynamic
    construct DynamicType [Type]
        'JSDynamicObject
    by
        '( 'new ClassId '(: DynamicType ) Arguments ') '(: DynamicType )
end rule

rule _dynamicNewObjectTypes
    % New expressions may create untyped dynamic objects if the resulting object's properties
    % are dynamically modified

    % Find every class declaration, and its following scope
    % (Class declarations are declaration-before-use in TS, so if it's used before that's a different problem)
    replace $ [SourceElement*]
        Modifiers [AccessibilityModifier*] 'class ClassId [Identifier] TypeParameters [TypeParameters?]
            ClassHeritage [ClassHeritage]
        '{ ClassBody [ClassElement*] '} End [EOB]
        Scope [SourceElement*]
    % Find every new expression using the class, and see if it actually creates an untyped dynamic object
    by
        Modifiers 'class ClassId TypeParameters ClassHeritage
        '{ ClassBody
        '} End
        Scope [_dynamicNewObjectClassTypes ClassId ClassBody Scope]
end rule

rule _dynamicNewObjectClassTypes ClassId [Identifier] ClassBody [ClassElement*] Scope [SourceElement*]
    % Find every new expression for the class
    replace $ [SimpleLexicalBinding]
        ObjectId [Identifier] Nullable [Nullable?] ': Type [Type] '= '( 'new ClassId '(: _ [Type] ) Arguments [Arguments] ') '(: _ [Type] )
    % Which is not already an untyped dynamic object
    deconstruct not Type
        'JSDynamicObject
    % But is used dynamically
    where
        Scope [_hasDynamicAssignmentProperty ObjectId ClassBody]
    % Explicitly type it as an untyped dynamic object
    construct DynamicType [Type]
        'JSDynamicObject
    by
        ObjectId Nullable ': DynamicType '= '( 'new ClassId '(: DynamicType ) Arguments ') '(: DynamicType )
end rule

function _hasDynamicAssignmentProperty ObjectId [Identifier] ClassBody [ClassElement*]
    % Is the object created by the new expression dynamically modified by an assigned property ...
    match * [LeftHandSideExpression]
        ObjectId '. PropertyId [Identifier] '(: _ [Type] )
    % ... that does not already exist in the class declaration?
    skipping [ClassBody]
    deconstruct not * [MemberVariableDeclaration] ClassBody
        _ [AccessibilityModifier*] PropertyId _ [Nullable ?] _ [TypeAnnotation?] _ [Initializer?] _ [EOS]
    skipping [ClassBody]
    deconstruct not * [MemberFunctionDeclaration] ClassBody
        _ [AccessibilityModifier*] _ ['async ?] _ ['* ?] _ [getOrSet?] PropertyId _ [Nullable ?] _ [CallSignature] _ [OptionalFunctionBody]
end function

