% JS2TS - convert JavaScript to TypeScript using type inference
% James Cordy, Huawei Technologies
% May 2022 (Rev. Aug 2023)

% Builtin class and function dynamic call type inference rules

% We can infer the expression type of function calls to a function
% of a built-in class object if the function returns a known type

% This rule set handles dynamic built-in class object properties and functions
% See builtin-function-types.rul for static built-in class properties and functions

function initializeBuiltinCallTypes
    % Inference rules need to be re-run on each iteration, but we only need to make this table once,
    % so it is separated into this initializer function

    % Table of the return types of dynamic (prototype) properties and functions
    % of the standard built-in classes

    construct BuiltinClassTypeMappings [_BuiltinClassTypeMapping*]
        % Regexp instance functions and properties
        'RegExp {
            'exec               ': 'string '[ '] '| 'null
            'dotAll             ': 'boolean
            'flags              ': 'string
            'global             ': 'boolean
            'hasIndices         ': 'boolean
            'ignoreCase         ': 'boolean
            'multiline          ': 'boolean
            'source             ': 'string
            'sticky             ': 'boolean
            'unicode            ': 'boolean
            'test               ': 'boolean
        }

        % Number instance functions and properties
        'Number {
            'toExponential      ': 'string
            'toFixed            ': 'string
            'toPrecision        ': 'string
            'valueOf            ': 'number
        }
        'number {
            'toExponential      ': 'string
            'toFixed            ': 'string
            'toPrecision        ': 'string
            'valueOf            ': 'number
        }

        % Boolean instance functions and properties
        'Boolean {
            'valueOf            ': 'boolean
        }
        'boolean {
            'valueOf            ': 'boolean
        }

        % Array instance Functions and Properties
        'Array {
        %   'at                 ': 'any
            'concat             ': 'any '[ ']
            'copyWithin         ': 'any '[ ']
            'entries            ': 'IterableIterator' < '[ 'number, 'any '] '>
            'every              ': 'boolean
            'fill               ': 'any '[ ']
            'filter             ': 'any '[ ']
        %   'find               ': 'any
            'findIndex          ': 'number
        %   'findLast           ': 'any
            'findLastIndex      ': 'number
            'flat               ': 'any '[ ']
            'flatMap            ': 'any '[ ']
            'includes           ': 'boolean
            'indexOf            ': 'number
            'join               ': 'string
            'keys               ': 'IterableIterator '< 'number '>
            'lastIndexOf        ': 'number
            'length             ': 'number
            'map                ': 'any '[ ']
        %   'pop                ': 'any
            'push               ': 'number
        %   'reduce             ': 'any
        %   'reduceRight        ': 'any
            'reverse            ': 'any '[ ']
        %   'shift              ': 'any
            'slice              ': 'any '[ ']
            'some               ': 'boolean
            'sort               ': 'any '[ ']
            'splice             ': 'any '[ ']
            'unshift            ': 'number
            'values             ': 'IterableIterator' < 'number '>
            'valueOf            ': 'any '[ ']
        }

        % String instance Functions and Properties
        'String {
            'anchor             ': 'string
            'at                 ': 'string '| 'undefined
            'big                ': 'string
            'blink              ': 'string
            'bold               ': 'string
            'charAt             ': 'string
            'charCodeAt         ': 'number
            'codePointAt        ': 'number '| 'undefined
            'concat             ': 'string
            'endsWith           ': 'boolean
            'fixed              ': 'string
            'fontColor          ': 'string
            'fontSize           ': 'string
            'includes           ': 'boolean
            'indexOf            ': 'number
            'italics            ': 'string
            'lastIndexOf        ': 'number
            'length             ': 'number
            'link               ': 'string
            'localeCompare      ': 'number
            'match              ': 'string '[ '] '| 'null
            'matchAll           ': 'IterableIterator '< 'string '[ '] '| 'null '>
            'normalize          ': 'string
            'padEnd             ': 'string
            'padStart           ': 'string
            'repeat             ': 'string
            'replace            ': 'string
            'replaceAll         ': 'string
            'search             ': 'number
            'slice              ': 'string
            'small              ': 'string
            'split              ': 'string '[ ']
            'startsWith         ': 'boolean
            'strike             ': 'string
            'sub                ': 'string
            'substr             ': 'string
            'substring          ': 'string
            'sup                ': 'string
            'toLocaleLowerCase  ': 'string
            'toLocaleUpperCase  ': 'string
            'trim               ': 'string
            'trimEnd            ': 'string
            'trimStart          ': 'string
            'valueOf            ': 'string
        }
        % Same for instances of primitive type string
        'string {
            'anchor             ': 'string
            'at                 ': 'string '| 'undefined
            'big                ': 'string
            'blink              ': 'string
            'bold               ': 'string
            'charAt             ': 'string
            'charCodeAt         ': 'number
            'codePointAt        ': 'number '| 'undefined
            'concat             ': 'string
            'endsWith           ': 'boolean
            'fixed              ': 'string
            'fontColor          ': 'string
            'fontSize           ': 'string
            'includes           ': 'boolean
            'indexOf            ': 'number
            'italics            ': 'string
            'lastIndexOf        ': 'number
            'length             ': 'number
            'link               ': 'string
            'localeCompare      ': 'number
            'match              ': 'string '[ '] '| 'null
            'matchAll           ': 'IterableIterator '< 'string '[ '] '| 'null '>
            'normalize          ': 'string
            'padEnd             ': 'string
            'padStart           ': 'string
            'repeat             ': 'string
            'replace            ': 'string
            'replaceAll         ': 'string
            'search             ': 'number
            'slice              ': 'string
            'small              ': 'string
            'split              ': 'string '[ ']
            'startsWith         ': 'boolean
            'strike             ': 'string
            'sub                ': 'string
            'substr             ': 'string
            'substring          ': 'string
            'sup                ': 'string
            'toLocaleLowerCase  ': 'string
            'toLocaleUpperCase  ': 'string
            'trim               ': 'string
            'trimEnd            ': 'string
            'trimStart          ': 'string
            'valueOf            ': 'string
        }

        % Date instance Functions and Properties
        'Date {
            'getDate            ': 'number
            'getDay             ': 'number
            'getFullYear        ': 'number
            'getHours           ': 'number
            'getMilliseconds    ': 'number
            'getMinutes         ': 'number
            'getMonth           ': 'number
            'getSeconds         ': 'number
            'getTime            ': 'number
            'getTimezoneOffset  ': 'number
            'getUTCDate         ': 'number
            'getUTCDay          ': 'number
            'getUTCFullYear     ': 'number
            'getUTCHours        ': 'number
            'getUTCMilliseconds ': 'number
            'getUTCMinutes      ': 'number
            'getUTCMonth        ': 'number
            'getUTCSeconds      ': 'number
            'setDate            ': 'number
            'setFullYear        ': 'number
            'setHours           ': 'number
            'setMilliseconds    ': 'number
            'setMinutes         ': 'number
            'setMonth           ': 'number
            'setSeconds         ': 'number
            'setTime            ': 'number
            'setUTCDate         ': 'number
            'setUTCFullYear     ': 'number
            'setUTCHours        ': 'number
            'setUTCMilliseconds ': 'number
            'setUTCMinutes      ': 'number
            'setUTCMonth        ': 'number
            'setUTCSeconds      ': 'number
            'toDateString       ': 'string
            'toISOString        ': 'string
            'toJSON             ': 'string
            'toLocaleDateString ': 'string
            'toLocaleTimeString ': 'string
            'toLocaleString     ': 'string
            'toTimeString       ': 'string
            'toUTCString        ': 'string
            'valueOf            ': 'string
        }

        % FinalizationRegistry instance functions and properties
        'FinalizationRegistry '< 'any '> {
            'register   ': 'void
            'unregister ': 'void
        }

        % Error instance functions and properties
        'Error {
            'name               ': 'string
            'message            ': 'string
            'stack              ': 'string '| 'undefined
        }

        % Intl subclass instance functions and properties
        'Intl.RelativeTimeFormat {
            'format             ': 'string
            'formatToParts      ': 'any '[ ']
        %   'resolvedOptions    ': 'any
        }

        'Intl.Collator {
        %   'resolvedOptions    ': 'any
            'compare            ': 'number
        }

        'Intl.DateTimeFormat {
            'format             ': 'string
            'formatRange        ': 'any '[ ']
            'formatRangeToParts ': 'any '[ ']
            'formatToParts      ': 'any '[ ']
        %   'resolvedOptions    ': 'any
        }

        'Intl.ListFormat {
            'format             ': 'string
            'formatToParts      ': 'any '[ ']
        %   'resolvedOptions    ': 'any
        }

        'Intl.NumberFormat {
            'format             ': 'string
            'formatRange        ': 'any '[ ']
            'formatRangeToParts ': 'any '[ ']
            'formatToParts      ': 'any '[ ']
        %   'resolvedOptions    ': 'any
        }

        'Intl.PluralRules {
        %   'resolvedOptions    ': 'any
            'select             ': 'string
            'selectRange        ': 'string
        }

        'Intl.Segmenter {
        %   'resolvedOptions    ': 'any
            'segment            ': 'string
        }

        % Generator instance functions and properties - need to be generalized
        'Generator '< 'number, 'any, 'any '> {
            'next   ': 'IteratorResult '< 'number, 'any '>
            'return ': 'IteratorResult '< 'number, 'any '>
            'throw  ': 'IteratorResult '< 'number, 'any '>
        }

        'Generator '< 'string, 'any, 'any '> {
            'next   ': 'IteratorResult '< 'string, 'any '>
            'return ': 'IteratorResult '< 'string, 'any '>
            'throw  ': 'IteratorResult '< 'string, 'any '>
        }

        'IteratorResult '< 'number, 'any '> {
            'value  ': 'number
            'done   ': 'boolean
        }

        'IteratorResult '< 'string, 'any '> {
            'value  ': 'string
            'done   ': 'boolean
        }

        % TypedArray instance functions and properties (for all of the TypedArray types)
        'TypedArray {
            'buffer             ': 'ArrayBuffer
            'byteLength         ': 'number
            'byteOffset         ': 'number
            'length             ': 'number
            'at                 ': 'number
            'copyWithin         ': 'TypedArray
            'entries            ': 'IterableIterator' < '[ 'number, 'any '] '>
            'every              ': 'boolean
            'fill               ': 'TypedArray
            'filter             ': 'TypedArray
            'find               ': 'number '| 'undefined
            'findIndex          ': 'number
            'findLast           ': 'number '| 'undefined
            'findLastIndex      ': 'number
            'forEach            ': 'undefined
            'includes           ': 'boolean
            'indexOf            ': 'number
            'join               ': 'TypedArray
            'keys               ': 'IterableIterator '< 'number '>
            'lastIndexOf        ': 'number
            'map                ': 'TypedArray
            'reduce             ': 'number
            'reduceRight        ': 'number
            'reverse            ': 'TypedArray
            'set                ': 'TypedArray
            'slice              ': 'TypedArray
            'some               ': 'boolean
            'sort               ': 'TypedArray
            'subarray           ': 'TypedArray
            'toLocaleString     ': 'string
            'toReversed         ': 'TypedArray
            'toSorted           ': 'TypedArray
            'toString           ': 'string
            'values             ': 'IterableIterator' < 'number '>
            'with               ': 'TypedArray
        }

        'any {
            % Universal Object dynamic functions and properties
            'toString           ': 'string
            'toLocaleString     ': 'string
            'toUpperCase        ': 'string
            'toLowerCase        ': 'string
            'length             ': 'number  % applies to most objects, but not necessarily all
            'name               ': 'string  % applies to most objects, but not necessarily all
            'message            ': 'string  % applies to most objects, but not necessarily all

            % Universal Window dynamic functions and properties
            'clearInterval      ': 'void
            'clearTimeout       ': 'void
            'setInterval        ': 'number
            'setTimeout         ': 'number
        }

    construct BuiltinTypedArrayTypeIds [Identifier*]
        % TypedArray built-in types - all of these share the same TypedArray properties and functions
        'Int8Array 'Uint8Array 'Uint8ClampedArray 'Int16Array 'Uint16Array 'Int32Array 'Uint32Array
        'Float32Array 'Float64Array 'BigInt64Array 'BigUint64Array

    match [program]
        P [program]

    export BuiltinClassTypeMappings
        BuiltinClassTypeMappings
            [_expandBuiltinTypedArrayClasses each BuiltinTypedArrayTypeIds] % Copy the TypedArray entry for each typed array class
            [_trimUnusedBuiltinClasses P]                                   % Optimize by removing unused built-in classes
end function

function builtinCallTypes
    % Inference rules for properties and functions of object instances of the built-in classes
    % Use of built-in classes is not scope dependent, so we just do the whole global scope at once
    replace [program]
        Scope [SourceElement*]
    export Scope
    by
        Scope [_builtinClassCallTypes]
end function

% Private

% Structure of the built-in class table

define _BuiltinClassTypeMapping
    % class { member () : type }
    [Type] '{  [_BuiltinMemberTypeMapping*] '} [NL]
end define

define _BuiltinMemberTypeMapping
    [FieldIdentifier] ': [KnownType]
end define

% Custom initialize the built-in class table for this input program

function _expandBuiltinTypedArrayClasses BuiltinTypedArrayTypeId [Identifier]
    % Copy the TypedArray class mapping table for each of the typed array built-in classes
    replace * [_BuiltinClassTypeMapping*]
        'TypedArray '{ TypedArrayMembers [_BuiltinMemberTypeMapping*] '}
        More [_BuiltinClassTypeMapping*]
    construct TypedArrayTypeId [Identifier]
        'TypedArray
    by
        TypedArrayTypeId        '{ TypedArrayMembers '}
        BuiltinTypedArrayTypeId '{ TypedArrayMembers [$ TypedArrayTypeId BuiltinTypedArrayTypeId] '}
        More
end function

rule _trimUnusedBuiltinClasses P [program]
    % Optimization:
    % Trim unused built-in classes from the built-in class table

    % These built-in types appear in pretty well every program
    construct UniversalTypes [Type*]
        'number 'boolean 'string 'any 'Array 'RegExp
    % These built-in types are implicit when generators are present
    construct UniversalParameterizedTypes [Identifier*]
        'Generator 'IteratorResult

    % If a built-in class identifier is never used in the program, we don't need its type mappings
    replace [_BuiltinClassTypeMapping*]
        BuiltinClassType [Type] '{  _ [_BuiltinMemberTypeMapping*] '}
        Rest [_BuiltinClassTypeMapping*]

    % Is it a universally common built-in class?
    deconstruct not * [Type] UniversalTypes
        BuiltinClassType
    deconstruct * [Identifier] BuiltinClassType
        BuiltinClassId [Identifier]
    deconstruct not * [Identifier] UniversalParameterizedTypes
        BuiltinClassId

    % Are there any references to the built-in class in the program?
    deconstruct not * [Identifier] P
        BuiltinClassId
    % If not, we don't need its type mappings
    by
        Rest
end rule

% Type inference for built-in class object instances
% Iterate over the entries in the built-in class table

function _builtinClassCallTypes
    % Iterate over each built-in class
    import BuiltinClassTypeMappings [_BuiltinClassTypeMapping*]
    replace [SourceElement*]
        Scope [SourceElement*]
    by
        Scope [_builtinClassMemberCallTypes each BuiltinClassTypeMappings]
end function

function _builtinClassMemberCallTypes BuiltinClassTypeMapping [_BuiltinClassTypeMapping]
    % For each built-in class, look for object instances using its properties and functions
    replace [SourceElement*]
        Scope [SourceElement*]
    deconstruct BuiltinClassTypeMapping
        BuiltinClassType [Type] '{  BuiltinMemberTypeMappings [_BuiltinMemberTypeMapping*] '}
    by
        Scope
              [_builtinMemberStaticCallType BuiltinClassType BuiltinMemberTypeMappings]
              [_builtinMemberCallTypeUniversal BuiltinClassType BuiltinMemberTypeMappings]
              [_builtinMemberCallType BuiltinClassType BuiltinMemberTypeMappings]
              [_builtinMemberCallTypeArray BuiltinClassType BuiltinMemberTypeMappings]
              [_builtinMemberExpressionType BuiltinClassType BuiltinMemberTypeMappings]
              [_builtinMemberNewCallType BuiltinClassType BuiltinMemberTypeMappings]
end function

rule _builtinMemberStaticCallType ClassType [Type] BuiltinMemberTypeMappings [_BuiltinMemberTypeMapping*]
    % Look for static instances of dynamic members of the class
    deconstruct ClassType
        ClassId [Identifier]

    % Find every untyped static reference to a member of the class
    skipping [ExpressionType]       % Optimization: no use looking inside types, and there are lots of them
    replace $ [PrimaryExpression]
        ClassId  '. MemberId [FieldIdentifier] '(: 'any )

    % Is its type in the class's mapping table?
    deconstruct * [_BuiltinMemberTypeMapping] BuiltinMemberTypeMappings
        MemberId ': MemberType [KnownType]

    % Yes, infer the reference's type
    by
        ClassId  '. MemberId '(: MemberType )
end rule

rule _builtinMemberCallTypeUniversal ClassType [Type] BuiltinMemberTypeMappings [_BuiltinMemberTypeMapping*]
    % Look for instances of dynamic members of all classes
    deconstruct ClassType
        'any

    % Find every untyped reference to a dynamic member of any class
    skipping [ExpressionType]       % Optimization: no use looking inside types, and there are lots of them
    replace $ [PrimaryExpression]
        ObjectId [Identifier]  '. MemberId [FieldIdentifier] '(: 'any )

    % Is its type in the universal mapping table?
    deconstruct * [_BuiltinMemberTypeMapping] BuiltinMemberTypeMappings
        MemberId ': MemberType [KnownType]

    % Yes, infer the reference's type
    by
        ObjectId  '. MemberId '(: MemberType )
end rule

rule _builtinMemberCallType ClassType [Type] BuiltinMemberTypeMappings [_BuiltinMemberTypeMapping*]
    % Look for instances of dynamic (prototype) members of the class

    % Universal members are handled specially
    deconstruct not ClassType
        'any

    % Find every untyped reference to a dynamic member of any class
    skipping [ExpressionType]       % Optimization: no use looking inside types, and there are lots of them
    replace $ [PrimaryExpression]
        ObjectId  [Identifier] '. MemberId [FieldIdentifier] '(: 'any )

    % Is its type in this class's mapping table?
    deconstruct * [_BuiltinMemberTypeMapping] BuiltinMemberTypeMappings
        MemberId ': MemberType [KnownType]

    % Is it in fact an object of this class?
    import Scope [SourceElement*]
    skipping [MemberComponents]     % Optimization: no use looking inside things for declarations
    deconstruct * [SimpleLexicalBinding] Scope
        ObjectId _ [Nullable?] ': ClassType _ [Initializer?]

    % Yes, infer the reference's type
    by
        ObjectId '. MemberId '(: MemberType )
end rule

rule _builtinMemberCallTypeArray ClassType [Type] BuiltinMemberTypeMappings [_BuiltinMemberTypeMapping*]
    % Look for instances of dynamic (prototype) members of the Array class used on arrays
    deconstruct ClassType
        'Array

    % Find every untyped reference to a dynamic member of any class
    skipping [ExpressionType]       % Optimization: no use looking inside types, and there are lots of them
    replace $ [PrimaryExpression]
        ObjectId  [Identifier] '. MemberId [FieldIdentifier] '(: 'any )

    % Is its type in this class's mapping table?
    deconstruct * [_BuiltinMemberTypeMapping] BuiltinMemberTypeMappings
        MemberId ': MemberType [KnownType]

    % Is it in fact an array?
    import Scope [SourceElement*]
    skipping [MemberComponents]     % Optimization: no use looking inside things for declarations
    deconstruct * [SimpleLexicalBinding] Scope
        ObjectId _ [Nullable?] ': _ [PrimaryType] '[ _ [Type?] '] _ [Initializer?]

    % Yes, infer the reference's type
    by
        ObjectId '. MemberId '(: MemberType )
end rule

rule _builtinMemberExpressionType ClassType [Type] BuiltinMemberTypeMappings [_BuiltinMemberTypeMapping*]
    % Look for expression instances of dynamic (prototype) members of the class

    % Universal members are handled specially
    deconstruct not ClassType
        'any

    % Find every untyped expression reference to a dynamic member of this class
    skipping [ExpressionType]       % Optimization: no use looking inside types, and there are lots of them
    replace $ [PrimaryExpression]
        '( SubPrimaryExpression [SubPrimaryExpression] '(: ClassType )  '. MemberId [FieldIdentifier] ') '(: 'any )

    % Is its type in this class's mapping table?
    deconstruct * [_BuiltinMemberTypeMapping] BuiltinMemberTypeMappings
        MemberId ': MemberType [KnownType]

    % Yes, infer the expression reference's type
    by
        '( SubPrimaryExpression '(: ClassType )  '. MemberId ') '(: MemberType )
end rule

rule _builtinMemberNewCallType ClassType [Type] BuiltinMemberTypeMappings [_BuiltinMemberTypeMapping*]
    % Look for "new" expression instances of dynamic (prototype) members of the class
    deconstruct ClassType
        ClassId [Identifier]

    % Find every untyped new expression reference to a dynamic member of this class
    skipping [ExpressionType]       % tuning: no use looking inside types
    replace $ [PrimaryExpression]
        '( 'new '( ClassId '(: ClassId ) ClassArguments [Arguments] ') '(: ClassId ) '. MemberId [FieldIdentifier] MemberArguments [Arguments] ') '(: 'any )

    % Is its type in this class's mapping table?
    deconstruct * [_BuiltinMemberTypeMapping] BuiltinMemberTypeMappings
        MemberId ': MemberType [KnownType]

    % Yes, infer the new expression reference's type
    by
        '( 'new '( ClassId '(: ClassId ) ClassArguments ') '(: ClassId ) '. MemberId MemberArguments ') '(: MemberType )
end rule
