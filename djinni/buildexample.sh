CPP_OUTPUT_FOLDER=target/djinni-example/cpp
ARKTS_OUTPUT_FOLDER=target/djinni-example/arkts
MY_PROJECT=target/djinni-example/proj.djinni


src/run  --arkts-out $ARKTS_OUTPUT_FOLDER --arkts-type-prefix Node --arkts-module DjinniExample --arkts-include-cpp $CPP_OUTPUT_FOLDER --cpp-optional-header absl/types/optional.h --cpp-optional-template absl:optional --cpp-out $CPP_OUTPUT_FOLDER --idl $MY_PROJECT 
