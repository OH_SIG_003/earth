// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from proj-rrr.djinni

#include "NodeMyCppInterfaceCpp.hpp"
#include "MyCppInterface.hpp"

#include "common.h"
#include <string>

#include <hilog/log.h>

napi_ref NodeMyCppInterface::listener_ref;
napi_ref NodeMyCppInterface::constructor;
napi_threadsafe_function NodeMyCppInterface::tsfn;

struct promiseInfo
{
    napi_async_work worker;
    napi_deferred deferred;

    string from;
    string to;
    int32_t sayType;

    int32_t result;
    string errMsg;
    string response;
};

struct promiseInfo dataInstance = {nullptr, nullptr, "", "", 0, 0, "", ""};
struct promiseInfo *ptr = &dataInstance;

void work(napi_env env, void *data)
{
    struct promiseInfo *arg = (struct promiseInfo *)data;
    int sum = 100;
    arg->result = sum;
    arg->errMsg = "native error";
    arg->response = "native response";
}

void done(napi_env env, napi_status status, void *data)
{
    struct promiseInfo *arg = (struct promiseInfo *)data;
    OH_LOG_INFO(LOG_APP, "Test C++ DONE Resolve Promise number: %{public}d", arg->result);
    napi_value ret;
    napi_create_int32(env, arg->result, &ret);
    napi_resolve_deferred(env, arg->deferred, ret);
    napi_delete_async_work(env, arg->worker);
    arg->deferred = nullptr;
}
napi_value NodeMyCppInterface::method_returning_nothing(napi_env env, napi_callback_info info) {
    // Check if method called with right number of arguments


    size_t argc = 1;
    napi_value argv[1] = {0};
    napi_value thisVar = nullptr;
    void *data = nullptr;
    NODE_API_CALL(env, napi_get_cb_info(env, info, &argc, argv, &thisVar, &data));
    NODE_API_ASSERT(env, argc == 1, "NodeMyCppInterface::method_returning_nothing needs 1 arguments");

    // Check if parameters have correct types
    napi_valuetype valuetype0;
    NODE_API_CALL(env, napi_typeof(env, argv[0], &valuetype0));
    NODE_API_ASSERT(env, valuetype0 == napi_number, "Wrong type of arguments. Expects an enum as Position- 0 argument.");

    int32_t argv_0;
    size_t value_length;
    NODE_API_CALL(env, napi_get_value_int32(env, argv[0], &argv_0));

    // Unwrap current object and retrieve its Cpp Implementation
    MyCppInterface *cpp_impl;
    NODE_API_CALL(env, napi_unwrap(env, thisVar, reinterpret_cast<void **>(&cpp_impl)));
    NODE_API_ASSERT(env, cpp_impl, "NodeMyCppInterface::method_returning_nothing : implementation of MyCppInterface is not valid");
    return nullptr;

}
napi_value NodeMyCppInterface::method_returning_some_type(napi_env env, napi_callback_info info) {
    // Check if method called with right number of arguments


    size_t argc = 1;
    napi_value argv[1] = {0};
    napi_value thisVar = nullptr;
    void *data = nullptr;
    NODE_API_CALL(env, napi_get_cb_info(env, info, &argc, argv, &thisVar, &data));
    NODE_API_ASSERT(env, argc == 1, "NodeMyCppInterface::method_returning_some_type needs 1 arguments");

    // Check if parameters have correct types
    napi_valuetype valuetype0;
    NODE_API_CALL(env, napi_typeof(env, argv[0], &valuetype0));
    NODE_API_ASSERT(env, valuetype0 == napi_string, "Wrong type of arguments. Expects a string as Position- 0 argument.");

    char argv_0[256] = "";
    size_t value_length;
    NODE_API_CALL(env, napi_get_value_string_utf8(env, argv[0], argv_0, 255, &value_length));

    // Unwrap current object and retrieve its Cpp Implementation
    MyCppInterface *cpp_impl;
    NODE_API_CALL(env, napi_unwrap(env, thisVar, reinterpret_cast<void **>(&cpp_impl)));
    NODE_API_ASSERT(env, cpp_impl, "NodeMyCppInterface::method_returning_some_type : implementation of MyCppInterface is not valid");
    return nullptr;

}
void NodeMyCppInterface::Destructor(napi_env env, void *nativeObject, void * /*finalize_hint*/)
{
    NodeMyCppInterface *obj = static_cast<NodeMyCppInterface *>(nativeObject);
    obj = NULL;
    return;
}


void NodeMyCppInterface::Init(napi_env env, napi_value exports) {
    napi_property_descriptor properties[] =  {
        DECLARE_NODE_API_PROPERTY("method_returning_nothing", method_returning_nothing),
        DECLARE_NODE_API_PROPERTY("method_returning_some_type", method_returning_some_type),
    };

    napi_value cons;
    NODE_API_CALL_RETURN_VOID(env, napi_define_class(env, "NodeMyCppInterface", -1, New, nullptr,
	sizeof(properties) / sizeof(napi_property_descriptor), properties,
	&cons));

    NODE_API_CALL_RETURN_VOID(env, napi_create_reference(env, cons, 1, &constructor));

    NODE_API_CALL_RETURN_VOID(env, napi_set_named_property(env, exports, "NodeMyCppInterface", cons));
}

napi_value NodeMyCppInterface::New(napi_env env, napi_callback_info info) {
    napi_value new_target;
    NODE_API_CALL(env, napi_get_new_target(env, info, &new_target));
    bool is_constructor = (new_target != nullptr);

    size_t argc = 1;
    napi_value args[1];
    napi_value _this;
    NODE_API_CALL(env, napi_get_cb_info(env, info, &argc, args, &_this, nullptr));

    if (is_constructor)
    {
        MyCppInterface* cpp_instance;

        NODE_API_CALL(env, napi_wrap(env, _this, cpp_instance, NodeMyCppInterface::Destructor, nullptr, nullptr));

        return _this;
    }

    argc = 1;
    napi_value argv[1] = {args[0]};

    napi_value cons;
    NODE_API_CALL(env, napi_get_reference_value(env, constructor, &cons));

    napi_value instance;
    NODE_API_CALL(env, napi_new_instance(env, cons, argc, argv, &instance));

    return instance;
}

