// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from proj-rrr.djinni

#include "NodeMyTsInterface.hpp"
#include "NodeHexUtils.hpp"
#include "common.h"

napi_ref NodeMyTsInterface::constructor;

NodeMyTsInterface::NodeMyTsInterface() : env_(nullptr), wrapper_(nullptr) {}

NodeMyTsInterface::~NodeMyTsInterface() { napi_delete_reference(env_, wrapper_); }

void NodeMyTsInterface::Destructor(napi_env env, void *nativeObject, void * /*finalize_hint*/)
{
    NodeMyTsInterface *obj = static_cast<NodeMyTsInterface*>(nativeObject);
    obj = NULL;
    return;
}

void NodeMyTsInterface::Init(napi_env env, napi_value exports) {
    napi_property_descriptor properties[] =  {
        DECLARE_NODE_API_PROPERTY("log_int", log_int),
    };

    napi_value cons;
    NODE_API_CALL_RETURN_VOID(env, napi_define_class(env, "NodeMyTsInterface", -1, New, nullptr,
	sizeof(properties) / sizeof(napi_property_descriptor), properties,
	&cons));

    NODE_API_CALL_RETURN_VOID(env, napi_create_reference(env, cons, 1, &constructor));

    NODE_API_CALL_RETURN_VOID(env, napi_set_named_property(env, exports, "NodeMyTsInterface", cons));
}

napi_value NodeMyTsInterface::New(napi_env env, napi_callback_info info) {
    napi_value new_target;
    NODE_API_CALL(env, napi_get_new_target(env, info, &new_target));
    bool is_constructor = (new_target != nullptr);

    size_t argc = 1;
    napi_value args[1];
    napi_value _this;
    NODE_API_CALL(env, napi_get_cb_info(env, info, &argc, args, &_this, nullptr));

    if (is_constructor)
    {
        MyTsInterface* cpp_instance;

        NODE_API_CALL(env, napi_wrap(env, _this, cpp_instance, NodeMyTsInterface::Destructor, nullptr, nullptr));

        return _this;
    }

    argc = 1;
    napi_value argv[1] = {args[0]};

    napi_value cons;
    NODE_API_CALL(env, napi_get_reference_value(env, constructor, &cons));

    napi_value instance;
    NODE_API_CALL(env, napi_new_instance(env, cons, argc, argv, &instance));

    return instance;
}


void NodeMyTsInterface::log_int(int32_t str)
{
    int32_t str;
    napi_call_threadsafe_function(NodeMyTsInterface::log_intThreadSafe, &data0, napi_tsfn_blocking);

    return;
    auto arg_0 = Nan::To<int32_t>(info[0]).FromJust();
}


