#include "napi/native_api.h"
#include "NodeISayHelloCpp.hpp"
#include "NodeISayHelloListener.hpp"
#include "NodeISayHelloCallback.hpp"

EXTERN_C_START
static napi_value Init(napi_env env, napi_value exports)
{
    NodeISayHello::Init(env, exports);
    NodeISayHelloListener::Init(env, exports);

    return exports;
}
EXTERN_C_END

static napi_module callbackModule = 
{
    .nm_version = 1,
    .nm_flags = 0,
    .nm_filename = nullptr,
    .nm_register_func = Init,
    .nm_modname = "entry",
    .nm_priv = ((void*)0),
    .reserved = {0},
};

extern "C" __attribute__((constructor)) void CallbackTestRegister()
{
    napi_module_register(&callbackModule);
}
