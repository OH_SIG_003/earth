// OpentTxl-C Version 11 rule compiler
// Copyright 2023, James R Cordy and others

#ifdef TXLMAIN
// Initialization in txl.c main process
extern void ruleCompiler (void);

// Used only in txl.c main process
extern void ruleCompiler_makeRuleTable (const treePT txlParseTreeTP);
#endif
