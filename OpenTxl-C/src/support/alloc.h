// OpentTxl-C Version 11 safe C dynamic arrays
// Copyright 2023, James R. Cordy and others

// 11.3.6   Revised to free all allocated arrays to support embedded applications

// Allocated array table
#define tamaxArrays 100

// Usage:  array(type, name);
#define array(type, name)   type *name

// Usage:  arrayalloc (upper, type, name);
#define arrayalloc(size, type, name)    name = (type *) (taarrayalloc(size, sizeof (type)))
extern void * taarrayalloc (const int arraysize, const int typesize);

// Safe non-scalar assignment - do not use memcpy, memset, memmove
#define arrayassign(dest,src)   arraycpy((void *)&(dest), (void *)&(src), (sizeof(dest)))
#define structassign(dest,src)  arraycpy((void *)&(dest), (void *)&(src), (sizeof(dest)))
extern void arraycpy(const void *dest, const void *src, unsigned int size);

// Support routines
extern void tainitialize (void);
extern void tafinalize (void);

// Undefine dangerous C mem library so that we don't accidentally use it
#undef memcpy
#undef memset
#undef memmove
