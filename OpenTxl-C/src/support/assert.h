// OpentTxl-C Version 11 safe C assertion checker
// Copyright 2023, James R. Cordy and others

// Assertions
#ifdef CHECKED
    #define assert(cond) {if (!(cond)) {fprintf (stderr, "Assertion failed, line %d file %s\n", __LINE__, __FILE__); throw (ASSERTFAIL);}}
#else
    #define assert(cond) {}
#endif

// Stack use limitation - to avoid crashes
typedef unsigned long long addressint;
#define checkstack() {int dummy; if ((stackBase - ((addressint)&(dummy))) > (addressint) maxStackUse) throw (STACKLIMIT);}
