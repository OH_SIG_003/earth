// OpentTxl-C Version 11 safe C arithmetic support
// Copyright 2023, James R Cordy and others

#undef min
#define min(a,b)        ((((int)a)<((int)b))?((int)a):((int)b))
#undef max
#define max(a,b)        ((((int)a)>((int)b))?((int)a):((int)b))
#undef round
#define round(realval)  ((int) realval)

