// OpentTxl-C Version 11 garbage collector
// Copyright 2023, James R Cordy and others

// Used only in xform.c transformer
#ifdef XFORM
extern void garbageRecovery_recoverGarbage (void);
#endif
