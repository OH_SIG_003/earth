// OpentTxl-C Version 11 grammar analyzer
// Copyright 2023, James R. Cordy and others

// Used only by compdef.c
#ifdef COMPILER
extern void analyzeGrammar (void); 
extern void analyzeGrammar_initialize (void);
extern void analyzeGrammar_checkAdjacentCombinatorialAmbiguity (const treePT defineTP, const treePT grammarTP);
extern void analyzeGrammar_checkEmbeddedCombinatorialAmbiguity (const treePT defineTP, const treePT grammarTP);
extern void analyzeGrammar_checkRepeatEmptyAmbiguity (const treePT defineTP, const treePT grammarTP);
extern void analyzeGrammar_checkHiddenLeftRecursionAmbiguity (const treePT defineTP, const treePT grammarTP);
#endif
