// OpentTxl-C Version 11 pre-hashed tokens and names
// Copyright 2023, James R Cordy and others

#ifdef TXLMAIN
// Initialization
extern void shared (void);
#endif

// Terminal tokens of the TXL language itself
extern tokenT empty_T, comma_T, dot_T, anonymous_T;
extern tokenT NL_T, FL_T, IN_T, EX_T, SP_T, TAB_T, SPOFF_T, SPON_T;
extern tokenT ATTR_T, KEEP_T, FENCE_T, SEE_T, NOT_T;
extern tokenT quote_T, underscore_T, openbracket_T, include_T, compounds_T, comments_T, keys_T, tokens_T, end_T, 
    redefine_T, bar_T, quit_T, assert_T, ignore_T, each_T, replace_T, match_T, any_T, star_T, dollar_T, dotDotDot_T;

// Terminal token type names
extern tokenT stringlit_T, charlit_T, token_T, key_T, number_T, floatnumber_T, decimalnumber_T, integernumber_T, 
    comment_T, id_T, upperlowerid_T, upperid_T, lowerupperid_T, lowerid_T, space_T, newline_T, 
    srclinenumber_T, srcfilename_T, undefined_T;

// TXL built-ins
extern tokenT TXL_optBar_T, TXLargs_T, TXLprogram_T, TXLinput_T, TXLexitcode_T;

// Pre-allocated shared common terminal token trees 
extern treePT emptyTP;
extern treePT commaTP;

// Type name to terminal type maps
extern enum treeKindT typeKind (tokenT name);
extern tokenT kindType[lastTreeKind + 1];

// Blank string for indenting
extern string BLANKS;
