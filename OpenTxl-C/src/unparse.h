// OpentTxl-C Version 11 unparser
// Copyright 2023, James R. Cordy and others

#ifdef TXLMAIN
// Initialization
extern void unparser (void);
#endif

// Routines for printing parse and grammar trees
extern void unparser_printParse (const treePT parseTP, const int outstream, const int indentation);
extern void unparser_printGrammar (const treePT grammarTP, const int indentation);
#ifndef STANDALONE
extern void unparser_printPatternParse (const treePT parseTP, const struct ruleLocalsT *localVars, const int indentation);
#endif

// Routines for unparsing output
extern void unparser_printLeaves (const treePT subtreeTP, const int outstream, const bool nl);
extern void unparser_printMatch (const treePT subtreeTP, const treePT matchTreeTP, const int outstream, const bool nl);

// Function to implement the [quote] and [unparse] predefined externals
extern void unparser_quoteLeaves (const treePT subtreeTP, longstring resultText);

// Procedure to extract the leaves from a tree to implement the [reparse] predefined external
extern void unparser_extractLeaves (const treePT subtreeTP);
