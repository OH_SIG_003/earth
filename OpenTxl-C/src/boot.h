// OpentTxl-C Version 11 bootstrap
// Copyright 2023, James R Cordy and others

#ifdef TXLMAIN
// Bootstrap the TXL language grammar
extern void bootstrap_makeGrammarTree (treePT *grammarTreeTP);
#endif
