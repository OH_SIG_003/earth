// OpentTxl-C Version 11 safe C support for I/O, strings, memory allocation and exception handling
// Copyright 2023, James R. Cordy and others

// Standard library dependencies
#include <stdbool.h>
#include <stdio.h>
#include <math.h>

// Memory allocation
#include "support/alloc.h"

// Exception codes
#include "support/exceptions.h"

// Exception handling 
#include "support/trycatch.h"

// Assertions
#include "support/assert.h"

// Arithmetic
#include "support/arith.h"

// Safe strings
#include "support/strings.h"

// Safe I/O
#include "support/io.h"

// Safe time
#include "support/time.h"

