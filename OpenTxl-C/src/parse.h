// OpentTxl-C Version 11 parser
// Copyright 2023, James R Cordy and others

// Initialization 
#ifdef TXLMAIN
extern void parser (void);
#endif

// Used in comprul.c rule compiler and parse.c parser itself
typedef void parser_parseVarOrExpProc (const treePT patternTokensTP, struct ruleLocalsT *localVarsAddr, const treePT productionTP, 
    treePT *parseTP, bool *isVarOrExp, bool *varOrExpMatches);

// Used in txl.c main process and xform_predef.h built-in functions
extern void parser_initializeParse (const string context, const bool isMain, const bool isPattern, const bool isTxl,
        struct ruleLocalsT *localVarsAddr, parser_parseVarOrExpProc *parseVarOrExp);
extern void parser_parse (const treePT productionTP, treePT *parseTP);

