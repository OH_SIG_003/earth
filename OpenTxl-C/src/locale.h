// OpentTxl-C Version 11 platform localization
// Copyright 2023, James R Cordy and others

#ifdef TXLMAIN
// Initialization
extern void locale (void);
#endif

// Platform-dependent paths
#ifdef WIN 
    #define defaultLibrary "C:/TXL/LIB"
    #define directoryChar  "/"          // (sic) - used by VS C
#else
    #define defaultLibrary "/usr/local/lib/txl"
    #define directoryChar  "/"
#endif

// Maximum output line length (can be changed using command line option)
#define defaultOutputLineLength 256

// Default size limit (approximate, in Mb)
#define defaultTxlSize 128

// Available stack space
extern addressint stackSize;
