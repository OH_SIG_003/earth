// OpentTxl-C Version 11 platform localization
// J.R. Cordy, Jan 2023

// Copyright 2023, James R. Cordy and others

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
// and associated documentation files (the “Software”), to deal in the Software without restriction, 
// including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies 
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
// AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// Modification Log

// v11.0 Initial revision, adapted from OpenTxl 11.0

#include <time.h>

// I/O, strings, memory allocation
#include "support.h"

// Check interface consistency
#include "locale.h"

// Available stack space
addressint stackSize;
#ifndef WIN
    #include <sys/resource.h>
#endif

addressint txl_stacksize (void)
{
    #define TXL_DEFAULT_STACKSIZE  8388608              /* 8Mb */
    unsigned long stacksize = TXL_DEFAULT_STACKSIZE;    /* We link at this size on Windows */
    
    #ifndef WIN
        /* See what we have */
        struct rlimit limit;
        int errcode = getrlimit (RLIMIT_STACK, &(limit));
        assert (errcode == 0);
        /* If it's not enough, ask for our default */
        if (limit.rlim_cur < TXL_DEFAULT_STACKSIZE) {
            limit.rlim_cur = TXL_DEFAULT_STACKSIZE;
            errcode = setrlimit (RLIMIT_STACK, &(limit));
        };
        /* See what we really have now */
        errcode = getrlimit (RLIMIT_STACK, &(limit));
        stacksize = limit.rlim_cur;
    #endif

    return (stacksize);
}

// Initialization
void locale (void)
{
    stackSize = (addressint) txl_stacksize ();
}
